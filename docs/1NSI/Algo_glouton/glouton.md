<center>
# Algorithmes Gloutons
</center>


## 1. Situation : Le probleme du sac à dos

Un cambrioleur possède un sac à dos d'une contenance maximum de 30 Kg. 

Au cours d'un de ses cambriolages, il a la possibilité de dérober 4 objets A, B, C et D. 

Voici un tableau qui résume les caractéristiques de ces objets :

<center>

|objet|A|B|C|D|
|-----|:-:|:-:|:-:|:-:|
|masse|13 Kg|12 Kg|8 Kg|10 Kg|
|valeur marchande|700 €|400 €|300 €|300 €|

</center>

!!! example "Problème à résoudre"

	Déterminer les objets que le cambrioleur aura intérêt à dérober, sachant que :

    - tous les objets dérobés devront tenir dans le sac à dos (30 Kg maxi)
    - le cambrioleur cherche à obtenir un gain maximum.

!!! warning "Remarque"

	Ce genre de problème est un grand classique en informatique, on parle de **problème d'optimisation**.
	
## 2. Problème d'optimisation


### 1. Définition

!!! prop "Définition"

	Un problème d'optimisation possède deux caractéristiques :

	- Une fonction que l'on doit maximiser ou minimiser.

		
		
	- une série de contraintes auxquelles il faut satisfaire.

		
		
??? example "Exemple du sac à dos"

	- La fonction à maximiser est celle qui calcule la valeur totale.
	- La seule contrainte est que le poids total ne doit pas excéder 30 kg.


### 2. Algorithme par force brute

Il suffit d'énumérer toutes les combinaisons possibles  (d'où le terme de force brute) afin de trouver la meilleure.
	
Pour l'exemple, le nombres de combinaisons est $2^4=16$.
	
??? "listes des combinaisons"
	
	|Objet|A|A+B|A+C|A+D|A+B+C|A+B+D|A+C+D|A+B+C+D|B|B+C|B+D|B+C+D|C|C+D|D|Aucun objet|
	|-|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
	|masse|13|25|21|23|33|35|31|43|12|20|22|30|8|18|10|0|
	|valeur|700|1100|1000|1000|1400|1400|1300|1700|400|700|700|1000|300|600|300|0|

Seules 11 combinaisons parmi ces 16 vérifient la contrainte du  poids et parmi celles-ci, la valeur la plus grande est 1100 € pour la combinaison A+B.

!!! warning "Remarque"
		
	Pour un nombre d'objet égal à $n$, cette méthode est en $o(a^n)$ avec $a$ un nombre, c'est à dire une complexité exponentielle. 
	
	Elle n'est donc pas utilisable par un algorithme pour $n$ grand pour une question de coût (notamment en temps).
	
Par conséquent, une alternative intéressante est l'algorithme glouton.

## 3. Algorithme glouton

Résoudre le problème revient à trouver un **choix optimal**, c'est à dire le choix le meilleur de tous.

Néanmoins, dans un algorithme, on peut choisir à chaque étape le choix optimal local, c'est à dire le meilleur choix à l'instant précis de cette étape.

!!! prop "Définition"

	Un algorithme glouton est un algorithme qui effectue à chaque étape, le meilleur choix possible parmi les choix disponibles à cet instant.

!!! warning "Remarque"

	Pour appliquer un algorithme glouton, il faut donc définir la manière de faire le meilleur choix local.
	
	Pour notre exemple, il y a trois possibilités :
	
	- **Choix 1** : choisir l'objet ayant le plus de valeur logeant dans le sac à dos _(pour avoir les objets les plus chers)_
	- **Choix 2** : choisir l'objet le moins lourd logeant dans le sac à dos _(pour avoir le plus d'objets)_
	- **Choix 3** : choisir l'objet ayant le meilleur rapport valeur/masse logeant dans le sac à dos

!!! example  "Résolution par algorithme glouton"

	=== "Avec le choix 1"

		- **étape 1** : On choisit l'objet le plus cher logeant dans le sac à dos :
		
			- c'est l'objet **A** avec un poids de 13 kg et une valeur de 700€.
			
			- Il reste donc $30-13=17$ kg disponibles dans le sac
			
		- **étape 2** : On choisit l'objet le plus cher parmi les restants avec un poids inférieur à 17 kg :
		
			- c'est l'objet **B** avec un poids de 12 kg et une valeur de 400€
			
			- Il reste donc $17-12=5$ kg disponibles dans le sac
			
		- **étape 3** : On choisit l'objet le plus cher parmi les restants avec un poids inférieur à 5 kg :
		
			- Il n'y aucun objet qui satisfait la contrainte : Fin de l'algorithme.
			
		La meilleure combinaison avec ce choix est donc **A+B** pour une valeur totale de 1100€.
		
	=== "Avec le choix 2"

		- **étape 1** : On choisit l'objet le moins lourd logeant dans le sac à dos :
		
			- c'est l'objet **C** avec un poids de 8 kg et une valeur de 300€
			
			- Il reste donc $30-8=22$ kg disponibles dans le sac
			
		- **étape 2** : On choisit l'objet le moins lourd parmi les restants avec un poids inférieur à 22 kg :
		
			c'est l'objet **D** avec un poids de 10 kg et une valeur de 300€
			
			Il reste donc $22-10=12$ kg disponibles dans le sac
		
		- **étape 3** : On choisit l'objet le moins lourd parmi les restants avec un poids inférieur à 12 kg :
		
			c'est l'objet **B** avec un poids de 12 kg et une valeur de 400€
			
			Il reste donc $12-12=0$ kg disponible dans le sac
			
		- **étape 4** : On choisit l'objet le moins lourd parmi les restants avec un poids inférieur à 0 kg :
		
			Il n'y aucun objet qui satisfait la contrainte : Fin de l'algorithme.
			
		La meilleure combinaison avec ce choix est donc **C+D+B** pour une valeur totale de 1000€.
		
	=== "Avec le choix 3"
	
		On calcule dans un premier temps, la qualité de chaque objet c'est à dire le rapport $\dfrac{valeur}{masse}$
		
		<center>
		
		|objet|A|B|C|D|
		|-|:-:|:-:|:-:|:-:|
		|qualité (€/kh)|54|33|38|30|
		
		</center>

		- **étape 1** : On choisit l'objet de meilleure qualité logeant dans le sac à dos :
		
			- c'est l'objet **A** avec un poids de 13 kg et une valeur de 700€
			
			- Il reste donc $30-13=17$ kg disponibles dans le sac
			
		- **étape 2** : On choisit l'objet de meilleure qualité parmi les restants avec un poids inférieur à 17 kg :
		
			- c'est l'objet **C** avec un poids de 8 kg et une valeur de 300€
			
			- Il reste donc $17-8=9$ kg disponibles dans le sac
			
		- **étape 3** : On choisit l'objet de meilleure qualité parmi les restants avec un poids inférieur à 9 kg :
		
			- Il n'y aucun objet qui satisfait la contrainte : Fin de l'algorithme.
			
		La meilleure combinaison avec ce choix est donc **A+C** pour une valeur totale de 1000€.

!!! warning "Remarque"

	Suivant la méthode de choix local, le résultat de l'algorithme glouton n'est pas nécessairement le meilleur choix global.
	

## 4. le problème du rendu de monnaie

![Caisse](images/rendu_monnaie.jpg){width=300 style="float:right"}

Vous êtes employé par l’entreprise CashExpress, spécialisée dans la vente de caisses enregistreuses automatiques : pour éviter les contacts entre les employés et la clientèle, le client dépose directement l’argent dans un automate qui se charge de lui rendre la monnaie

</br>

On suppose que la monnaie peut-être rendue avec toutes les pièces et billets existant en euros :



```python
monnaie = [200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1, 0.05, 0.01]
```

(En effet, les billets de 500€ ne sont plus produits depuis 2018)


1.  Un client vous présente un billet de 50€ pour payer sa chocolatine à 0,85€, indiquez le détail du rendu de monnaie que vous allez faire.  

2.  Proposez une stratégie vous permettant systématiquement de rendre la monnaie en utilisant un nombre minimal de pièces/billets.

3.  Écrire la fonction ```rendu_monnaie(montant)``` qui retourne un dictionnaire contenant le détail du rendu de monnaie pour un montant donné.

    !!! example "Exemple"

		- Le client a payé sa baguette à 1,05€ avec un billet de 5€ 
		- l’automate doit lui rendre 3,95€ 
		
		```python
		>>> rendu_monnaie(3,95)
		{2 :1 ,1 :1 ,0.5 :1 ,0.2 :2 ,0.05 :1}
		```
	
	En effet, le rendu optimal est 2€ + 1€ + 50ct + 2×20ct + 5ct  

4. Une boulangerie basée en Absurdistan souhaite se procurer une de vos machine. 

	Leur monnaie est le Plouk, dont le taux de change est proche de l’Euro : 1 Pk = 1 €.

	Leur monnaie est différemment échelonnée : 

	```python
	Monnaie_plouk=[200, 100, 30, 20, 5, 2, 1, 0.3, 0.1, 0.05, 0.01]
	```
	
	En adaptant la fonction rendu_monnaie(montant) aux différentes valeurs disponibles dans ce pays, quel retour propose-t-elle pour 45 Pk ? 

5.	Ce rendu de monnaie est-il optimal ?	
	
