<center>
# Algorithme des k-plus proches voisins
</center>

## 1. Introduction

Les algorithmes traditionnels visent à apporter efficacement des réponses correstes à des problèmes précisément définis. Les algorithme à base d'apprentissage (algorithmme machine learning) suivent une philosophie différentes : apporter une réponse plausible, mais pas nécessairement exacte à un problème auquel il est difficile d'appliquer un algorithme traditionnel. Le problème visé peut typiquement :

- être d'un complexité telle que le calcul d'une réponse exacte prendrait beaucopup trop de temps

    _Exemple : le choix du meilleur coup à jouer dans une partie de go_

- ne pas avoir de définition suffisamment précise

    _Exemple : le choix de la meilleure traduction en français d'une phrase en langue étrangère._

- être basé sur des données incomplètes ou imprécises.

    _Exemple : le choix de la meilleure publicité à montrer  à un utilisateur d'internet en fonction de ses goûts et de son humeur du moment._

Pour répondre à de tels problèmes, les algorithmes d'apprentissage ont besoin d'une grande quantité d'exemples associant des données d'entré et les réponses attendues, dont ils se servent pour essayer de deviner une réponse convenable lorsqu'on leur propose de nouvelles entrées.

## 2. Découverte de l'algorithme des k-plus proches voisins

L'algorithme des k-nn (k-nearest neighbors) ou k-plus proches voisins est un algorithme **machine learning**.

!!! Note "Vidéo explicative à visionner"

    <div class="youtube-player" data-id="9pvbEP1eyNY" maxwidth="600px"></div>	



!!! Example "Exercice"

    Voici une image qui donne la position de points bleus ou rouge.

    <center>
    ![prediction](images/image1.png){width=400 }

    </center>

    1. 
    
        === "Question"
    
            D'après l'algorithme des 3-ppv, de quelle couleur sera le point vert ? 
        
        === "Réponse"

            Parmi les trois plus proches voisins du point vert, il y a deux points rouge et un bleu : on estime donc que le point vert est rouge.

    2. 
    
        === "Question"
        
            D'après l'algorithme des 5-ppv, de quelle couleur sera le point vert ? 

        === "Réponse"

            Parmi les cinq plus proches voisins du point vert, il y a deux points rouge et trois bleu : on estime donc que le point vert est bleu.





!!! Example "Un exemple à manipuler" 

    On dispose dans un carré de cinquante points soit rouge soit vert.

    On souhaite prédire la couleur de n'importe quel point du carré en se servant des k points les plus proches.

    <center>
    Choisir le nombre des plus proches voisins étudiés : 
    <span class="box">
    <select name="knn" onchange="valeurk();" >
	<option>3</option>
	<option>5</option>
	<option>7</option>
    <span class="focus"></span>
    </select>
    </span>

    <div id="ggb-element"></div> 
    <form class="multi-button">
    <input name="button2" type="button"  class="button" onClick="liberer();"  value="Manuel">
    <input name="button4" type="button"  class="button" onClick="trace();"  value="Trace off">
    <input name="button1" type="button"  class="button" onClick="parcours();"  value="Auto">
    <input name="button3" type="button"  class="button" onClick="init2();"  value="Reset">
    </form>
    </center>

    ??? Note "Utilisation (cliquer pour déployer)"

        - Le bouton **Manuel** permet de déplacer le point M à l'aide la souris. 

            Le cercle indique la zone ou se trouvent les k plus proches voisins et détermine la couleur de M.
        
        - Le bouton **Trace** permet d'activer/désactiver la trace du point M.

        - Le bouton **Auto** permet de balayer la figure pour placer une centaine de point M. Il permet aussi de mettre le balayage en pause et de le reprendre.

        - Le bouton **Reset** per met de construire un nouveau jeu de points initiaux.

    Tester les différentes possibilités notamment en changeant la valeur de k.

<meta name=viewport content="width=device-width,initial-scale=2">
<script src="https://www.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
var params = {"id":"app1", "material_id":"ydsnxwkq", "enableShiftDragZoom":false,"showToolBar": false, "showAlgebraInput": false, "showMenuBar": false };
var liste= [];
var couleur=[];
var tr=false;
var pcrs=0;
var lib=false;
var button1 = document.getElementsByName("button1")[0];
var button4 = document.getElementsByName("button4")[0];
var k= document.getElementsByName("knn")[0].value;
function fin_cur(){
    if (app1.getValue("cur")==120)
        {
        button1.value="Auto";
        }
}
function valeurk(){
    k= document.getElementsByName("knn")[0].value;
    liberer();
    ldistance();
}
function parcours(){
    if (lib){lib=!lib;}
    if (app1.getValue("cur")==120)
        {pcrs=0;
        app1.refreshViews();
        button1.value="Auto";
        }
    if (pcrs==0){
        pcrs=1;
        app1.evalCommand("SetTrace(M,true)");
        //app1.unregisterUpdateListener("M");
        app1.evalCommand("cur=0");
        app1.setAnimationSpeed("cur",.25);
        app1.evalCommand("M=(Mod(cur,11)/10,Div(cur,11)/10)");
        app1.setAnimating("cur", true);
        app1.startAnimation();
        tr=true;
        button4.value="Trace on";
        button1.value="Pause";
                }
    else {
        if (pcrs==1){
            app1.stopAnimation();
            pcrs=2;
            button1.value="Reprendre";
        }
        else {
            app1.startAnimation();
            pcrs=1;
            button1.value="Pause";
        }
    }
}
function liberer(){
    if (!lib){
    pcrs=0;
    button1.value="Auto";
    app1.stopAnimation();
    app1.setAnimating("cur", false);
    app1.setAnimationSpeed("cur",1);
    app1.evalCommand("M=PointIn(q1)");
    app1.evalCommand("SetCoords( M, .5, .5 )");
    app1.refreshViews();
    lib=!lib;
    }
}
function distance(A,B){
        return app1.getValue("Distance("+A+","+B+")");
        }
function lsort(){
    ldist=[];
    for (let i=0;i<50;i=i+1){
        ldist.push({indice: i, distance: distance("A_{"+i+"}","M"),color : couleur[i]});}
    const sortByMapped = (map,compareFn) => (a,b) => compareFn(map(a),map(b));
    const byValue = (a,b) => a - b;
    const toPrice = e => e.distance;
    const byPrice = sortByMapped(toPrice,byValue);
    ldist2=[...ldist].sort(byPrice);
    couleurM=0;
    for (let i=0;i< k;i++){
        couleurM=couleurM+ldist2[i].color;
        }
    return [ldist2[k-1].distance,couleurM];
    }
function ldistance(){
    r=lsort()
    app1.evalCommand("C=Circle(M,"+r[0]+")");
    app1.evalCommand("Color="+r[1]);
    }
function init(){
for (let i=0;i<50;i=i+1){
    var x=Math.random() ;
    var y=Math.random();
    app1.evalCommand("A_{"+i+"}=("+x+","+y+")");
    app1.evalCommand("ShowLabel(A_{"+i+"},false)");
    app1.evalCommand("SetFixed(A_{"+i+"},true)");
    app1.evalCommand("SetPointStyle(A_{"+i+"},3)");
    if (y*y<x) {
        app1.evalCommand('SetColor(A_{'+i+'},"Red")');
        c=1;
        }
    else {
        app1.evalCommand('SetColor(A_{'+i+'},"Green")');
        c=-1;
        }
    couleur.push(c);
    }
}
function trace(){
    if (tr){button4.value="Trace off";}
    else{button4.value="Trace on";}
    tr=!tr;
    app1.evalCommand("SetTrace(M,"+tr+")");
}
function init2(){
    app1.refreshViews();
    liberer();
    couleur=[];
    for (let i=0;i<50;i=i+1){
    app1.evalCommand("SetFixed(A_{"+i+"},false)");
    }
    init();
    app1.evalCommand("M=PointIn(q1)");
    app1.evalCommand("SetCoords( M, .5, .5 )");
    ldistance();
}
params.appletOnLoad = function() {
    app1.setAxesVisible(false, false);
    app1.setGridVisible(false);
    app1.setVisible("q1",false);
    app1.evalCommand("SetTrace(M,"+tr+")");
    app1.evalCommand("ZoomIn(0,0,1,1)");
    init();
    liberer();
    ldistance();
    app1.registerObjectUpdateListener("M",ldistance);
    app1.registerObjectUpdateListener("cur",fin_cur);
}
var ggbApplet2 = new GGBApplet(params, true);
window.onload = function() {
    ggbApplet2.inject('ggb-element');}
</script>

!!! prop "Synthèse : Algorithme des k-nn"

    Pour utiliser l'algorithme des k-nn, on a donc besoin :

    - d'une table $T$ qui associe à chaque élément $e_i$ une classe $C_i$
    - d'une notion de distance entre deux éléments.

    ??? Example "Pour l'exemple précédent"

        - La table est l'ensemble des points associés à une couleur (soit rouge soit verte)

            cet exemple possède donc deux classes : la classe **rouge** et la classe **verte**

        - la distance est la distance euclidienne entre deux points A et B : $\sqrt{(x_A-x_B)^2+(y_A-y_B)^2}$

    L'algorithme dépend également d'un paramètre k, qui est un nombre entier strictement positif indiquant le nombre de données à prendre en compte.

    Pour estimer la classe d'un élément $e$, l'algorithme :

    1. cherche dans la table $T$ les $k$ éléments ayant les plus faibles distances à $e$.
    2. collecte les classes associées à ces $k$ éléments.
    3. détermine la classe la plus souvent présente parmi les classes retenues.

## 3. Travail à réaliser 



Soit le tableau `T` donné sous la forme `T=[[1,1,0],[2,3,1],[5,-1,0],[0.5,0,1],[3,4,1],[-1,1,0]]` ou chaque point est défini dans le tableau par `[abscisse,ordonnée,couleur]` avec 0 pour rouge et 1 pour noir.

On s'intéresse au point $X(0;3)$


1. Quelle sera sa couleur si on utilise l'algorithme des 3-nn?

    Réaliser un graphique.

2. Programmation en python 

    1. la distance utilisée sera celle vue en seconde : $AB=\sqrt{(x_A-x_B)^2+(y_A-y_B)^2}$

        Ecrire la fonction `distance` :

        ```python
        def distance(A,B :list)->float :
            '''
            - retourne la distance entre les points A et B
            - l'abscisse de A est donnée par A[0] et son ordonnée par A[1]
            '''
            return ...
        ```
        
    2. Ecrire l'algorithme du 3-nn en python.

3. Modifier l'algorithme précédent en k-NN

4. Envoyer votre algorithme par e-lyco