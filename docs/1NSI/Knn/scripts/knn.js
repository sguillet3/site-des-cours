function parcours(){
    app1.unregisterUpdateListener("M");
    app1.evalCommand("cur=0");
    app1.setAnimationSpeed("cur",.25);
    app1.evalCommand("M=(Mod(cur,11)/10,Div(cur,11)/10)");
    app1.setAnimating("cur", true);
    app1.startAnimation();
}
function liberer(){
    app1.evalCommand("M=PointIn(q1)");
}
function distance(A,B){
        return app1.getValue("Distance("+A+","+B+")");
        }
function lsort(){
    ldist=[];
    for (let i=0;i<50;i=i+1){
        ldist.push({indice: i, distance: distance("A_{"+i+"}","M"),color : couleur[i]});}
    const sortByMapped = (map,compareFn) => (a,b) => compareFn(map(a),map(b));
    const byValue = (a,b) => a - b;
    const toPrice = e => e.distance;
    const byPrice = sortByMapped(toPrice,byValue);
    ldist2=[...ldist].sort(byPrice);
    couleurM= ldist2[0].color+ldist2[1].color+ldist2[2].color;
    return [ldist2[2].distance,couleurM];
    }
function ldistance(){
    r=lsort()
    app1.evalCommand("C=Circle(M,"+r[0]+")");
    app1.evalCommand("Color="+r[1]);
    }
function init(){
for (let i=0;i<50;i=i+1){
    var x=Math.random() ;
    var y=Math.random();
    app1.evalCommand("A_{"+i+"}=("+x+","+y+")");
    app1.evalCommand("ShowLabel(A_{"+i+"},false)");
    app1.evalCommand("SetFixed(A_{"+i+"},true)");
    app1.evalCommand("SetPointStyle(A_{"+i+"},3)");
    if (y*y<x) {
        app1.evalCommand('SetColor(A_{'+i+'},"Red")');
        c=1;
        }
    else {
        app1.evalCommand('SetColor(A_{'+i+'},"Green")');
        c=-1;
        }
    couleur.push(c);
    }
}
function init2(){
    couleur=[];
    for (let i=0;i<50;i=i+1){
    app1.evalCommand("SetFixed(A_{"+i+"},false)");
    }
    init();
}