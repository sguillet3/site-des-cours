<center>
# STATISTIQUES
</center>
 

## Nuage de points

!!! Example "Méthode : Représenter un nuage de points"

    Le tableau suivant présente l’évolution du budget publicitaire et du chiffre d’affaire d’une société au cours des 6 dernières années :

    |Budget publicitaire en milliers d’euros $x_i$|8|10|12|14|16|18|
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    |Chiffre d’affaire en milliers d’euros $y_i$|40|55|55|70|75|95|

    === "Question"

        Dans un repère, représenter le nuage de points $(x_i  ; y_i )$.

    === "Réponse"

        <center>
        ![nuage de points](images/image1.png){width=500 }
        </center>

    === "Vidéo"

        <div class="youtube-player" data-id="Nn6uckb3RvE"></div>	

!!! prop "Définition :"

     L’ensemble des points $M_i$ de coordonnées $(x_i  ; y_i )$, est appelé le nuage de points associé à la série statistiques $(x_1  ; y_1 ),(x_2  ; y_2 ),…,(x_n  ; y_n )$  à deux variables. 


## II. Ajustement affine

### 1. Interpolation, extrapolation

L’objectif est, à partir des valeurs d’une série statistique à deux variables, d’obtenir des approximations pour des valeurs inconnues de cette série.

!!! Example "Exemples :"

    - On donne une série exprimant la population d’une ville en fonction des années et on souhaite faire des prévisions pour les années à venir.

    Les prévisions sortent du domaine d’étude de la série, on parle dans ce cas d’extrapolation.

    - On donne une série exprimant la température extérieure et la consommation électrique correspondante. Les températures étudiées s’échelonnent entre -10°C et 10°C avec un pas de 4°C.

    Sans faire de nouveaux relevés, on souhaite estimer la consommation électrique pour toutes les températures entières comprises entre -10°C et 10°C.

    Les calculs sont dans le domaine d’étude de la série, on parle dans ce cas d’interpolation.

!!! prop "Définitions :"
     L'interpolation et l’extrapolation sont des méthodes qui consistent à estimer une valeur inconnue dans une série statistique.

    - Pour une interpolation, le calcul est réalisé dans le domaine d'étude fourni par les valeurs de la série.
    - Pour une extrapolation, le calcul est réalisé en dehors du domaine d'étude.

!!! note "Remarque"
    La méthode d’extrapolation est parfois contestable car en dehors du domaine d’étude fourni par les valeurs de la série. Rien ne nous assure en effet que le modèle mathématique mis en œuvre soit encore valable.

### 2. Droite d’ajustement

Pour obtenir de telles estimations, il faudra déterminer une droite passant « le plus près possible » des points du nuage. 

L’interpolation ou l’extrapolation consiste à effectuer l’estimation par lecture graphique sur la droite ou par calcul à l’aide de l’équation de la droite.

!!! prop "Définition :"

    Lorsque les points d’un nuage sont sensiblement alignés, on peut construire une droite, appelé droite d’ajustement (ou droite de régression), passant « au plus près » de ces points.

Dans la suite, nous allons étudier différentes méthodes permettant d’obtenir une telle droite.

#### 1 - Méthode « au jugé »

Sans technique particulière, on trace « au jugé » une droite passant « au plus près » des points du nuage.
Cette méthode a le mérite d’être rapide mais pour être « juste », il faut un peu d’expérience.

<center>
![ajustement au jugé](images/image2.png){width=500 }
</center>

#### 2. Méthode des moindres carrés 



Cette méthode porte le nom de « moindre carrés » car elle consiste à rechercher la position de la droite d’ajustement tel que la somme des carrés des longueurs donnant les distances respectives (en vert) entre la droite et les points soit minimale.

La méthode consiste à déterminer les coefficients $a$ et $b$ d’une droite d’équation $y=ax+b$ de sorte qu’elle passe le « plus près possible » des points du nuage.

Pour chaque abscisse $x_i$, on calcule la distance $M_i P_i$ entre le point du nuage et le point de la droite, c'est à dire $y_i-(ax_i+b)$.

Il s’agit dans ce cas, de la droite d’ajustement de $y$ en $x$. 

??? "Illustration"

    <center>
    ![ajustement au jugé](images/image3.png){width=500 }
    </center>

Dans la méthode des moindres carrés, on recherche $a$ et $b$ pour lesquels la somme des carrés des distances est minimale.

!!! Example "Méthode : Déterminer la droite d’ajustement par la méthode des moindres carrés"

    On considère la série statistique à deux variables données dans le tableau suivant :

    |$x_i$|5|10|15|20|25|30|35|40|
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    |$y_i$|13|23|34|44|50|65|75|90| 

    ??? "Vidéo"

        <div class="youtube-player" data-id="vdEL0MOKAIg"></div>

    1.  === "Question"

            Dans un repère, représenter le nuage de points $(x_i ; y_i)$.

        === "Réponse"

            <center>
            ![Tracé](images/image4.png){width=500 }
            </center>

    2.  
    
        1.  === "Question"
            
                 À l’aide de la calculatrice, déterminer une équation de la droite d’ajustement par la méthode des moindres carrés.

            === "Réponse"

                === "Avec **TI** :"

                    - Appuyer sur **STATS** puis **Edite** et saisir les valeurs de $x_i$  dans **L1** et les valeurs de $y_i$ dans **L2**.
                    - Appuyer à nouveau sur **STATS** puis **CALC** et **RegLin(ax+b)**.
                    - Saisir **L1,L2**

                === "Avec **CASIO** :"

                    - Aller dans le menu **STAT**.
                    - Saisir les valeurs de $x_i$  dans **List1** et les valeurs de $y_i$ dans **List2**.
                    - Sélectionner **CALC** puis **SET**.
                    - Choisir **List1** pour **2Var XList** et **List2** pour **2Var YList** puis **EXE**.
                    - Sélectionner **REG** puis **X** et **aX+b**.

                === "Avec **Numworks** :"

                    - Aller dans le menu **Regression**
                    - Saisir les valeurs de $x_i$  dans **X1** et les valeurs de $y_i$ dans **Y1** dans l'onglet **Données**.
                    - Aller dans l'onglet **Stats**
                    - Dérouler la liste jusqu'à **Régression**

                La calculatrice nous renvoie : 
                
                $a=2.138095238$ et $b=1.142857143$
                
                Une équation de la droite d’ajustement est : 
                
                $$y=2,1x+1,1$$

        2.  === "Question"
            
                Représenter la droite d’ajustement de $y$ en $x$.

            === "Réponse"

                Pour tracer la droite, il suffit de calculer les coordonnées de deux points de la droite d’ajustement :

                - Si $x=0$ alors $y=2,1\times 0+1,1=1,1$ donc le point de coordonnées $(0 ;1,1)$ appartient à la droite d’ajustement.
                - Si $x=10$ alors $y=2,1\times 10+1,1=22,1$ donc le point de coordonnées $(10 ;22,1)$ appartient à la droite d’ajustement.

                Voir sur le graphique du 1
    3. 
    
        === "Question"
        
            Estimer graphiquement la valeur de $x$ pour $y = 70$. 
            
            Retrouver ce résultat par calcul.
            
            S’agit-il d’une interpolation ou d’une extrapolation ?

        === "Réponse"

            - Pour $y=70$, on lit graphiquement $x\simeq 33$.
            - Par calcul, si $y=70$, alors $70=2,1x+1,1$ Soit :
        
                $2,1x=70-1,1$

                $2,1x=68,9$

                $x=\dfrac{68,9}{2,1}\simeq 32,8$
                

            - Les calculs sont réalisés dans domaine d’étude, on parle donc d’interpolation.

## III. Ajustement par changement de variable

Lorsque le nuage de points n'est à priori pas modélisable par une droite, on peut réaliser un ajustement linéaire en effectuant un changement de variable.

!!! Example "Méthode : Effectuer un ajustement se ramenant par changement de variable à un ajustement affine "

    On a relevé la population d’une grande métropole sur 50 ans tous les 5 ans. Les résultats sont présentés dans le tableau suivant :

    |Année x_i|0|5|10|15|20|25|30|35|40|45|50|
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    |Population en milliers $y_i$|19,4|19,4|27,6|40,3|50|59|69|87|132|166|216|


    ??? "Vidéo"

        <div class="youtube-player" data-id="98L7oKSb0nA"></div>

    1.  === "Question"
            
            Représenter le nuage de points dans un repère.

        === "Réponse"

            <center>
            ![Tracé](images/image5.png){width=500 }
            </center>

    2. 
        1.  === "Question"
        
                On effectue le changement de variable $z=\log ⁡y$. Réaliser un nouveau tableau présentant les valeurs prises par les variables $x$ et $z$.

            === "Réponse"

                |$x_i$|0|5|10|15|20|25|30|35|40|45|50|
                |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
                |$z_i=\log⁡ y_i$|1,29|1,29|1,44|1,61|1,7|1,77|1,84|1,94|2,12|2,22|2,33|

        2.  === "Question"
        
                Représenter un nouveau nuage de points à partir des données des variables $x$ et $z$.

            === "Réponse"

                <center>
                ![Tracé](images/image6.png){width=500 }
                </center>

        3.  === "Question"
        
                A l’aide la calculatrice, déterminer une équation de la droite d’ajustement de $z$ en $x$ par la méthode des moindres carrés. 
                
                Représenter la droite d’ajustement.

            === "Réponse"

                Une équation de la droite d’ajustement est : 
                
                $z=0,0214x+1,2427$

                Tracé de la droite : voir sur le grapohique de la question précédente.

    3. 
        1.  === "Question"
        
                En déduire la relation qui lie y et x puis tracer la courbe représentative de la fonction f définie par y=f(x) dans le repère contenant le premier nuage de points.

            === "Réponse"

                On a : $z=0,0214x+1,2427$ et $z=\log ⁡y$, soit : 

                $\log ⁡y=0,0214x+1,2427$

                $10^{\log ⁡y} =10^{0,0214x+1,2427}$

                $y=10^{0,0214x+1,2427}$
                
                $y=10^{0,0214x}\times 10^{1,2427}$
                
                $y=17,4864\times 10^{0,0214x}$

                Donc :

                $f(x)=17,4864\times 10^{0,0214x}$ est l’expression de la fonction permettant d’ajuster le nuage de points $(x_i  ; y_i )$.

                On trace la courbe de $f$ ci-dessous.

                ??? "graphique"

                    <center>
                    ![Tracé](images/image7.png){width=500 }
                    </center>   

        2.  === "Question"
        
                En admettant que le modèle mathématique reste valable en dehors du domaine d’étude, extrapoler le nombre d’habitant 5 ans après l’étude.

            === "Réponse"

                $y=17,4864\times 10^{0,0214\times 55}\simeq 263$

                On peut supposer que 5 années après la fin de l’étude, la population de la ville sera proche de 263 000 habitants.




