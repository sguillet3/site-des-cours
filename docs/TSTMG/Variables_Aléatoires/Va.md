# VARIABLES ALÉATOIRES 


## I. Espérance d’une variable aléatoire

!!! prop "Définition :"

    Soir $X$ une variable aléatoire:
    
    L'espérance mathématique de $X$ est :

    $\displaystyle E(X) = x_1\times P(X=x_1 )+x_2\times P(X=x_2 )+\cdots+ x_n\times P(X=x_n)=\sum_{i=1}^n
    x_i\times P(X=x_i)$



    L'espérance est la moyenne que l'on peut espérer si l'on répète l'expérience un grand nombre de fois.

!!! Example "Méthode : Calculer l’espérance d’une variable aléatoire"

    ??? "Vidéo"

        <div class="youtube-player" data-id="AcWVxHgtWp4"></div>

    Soit l'expérience aléatoire : "On tire une carte dans un jeu de 32 cartes."

    On considère le jeu suivant :

    - Si on tire un cœur, on gagne 2 €.
    - Si on tire un roi, on gagne 5 €.
    - Si on tire une autre carte, on perd 1 €.

    On appelle $X$ la variable aléatoire qui à une carte tirée associe un gain ou une perte.

    1. 
        === "Question"
        
            Déterminer la loi de probabilité de $X$.

        === "Réponse"

            La variable aléatoire $X$ peut prendre les valeurs $2$, $5$, $–1$ mais aussi $7$.

            En effet, si on tire le roi de cœur, on gagne $5$(roi) + $2$(cœur) = $7 €$.

            - Si la carte tirée est un cœur (autre que le roi de cœur), $X = 2$.

                $P(X = 2) = \dfrac{7}{32}$.

            - Si la carte tirée est un roi (autre que le roi de cœur), $X = 5$.

                $P(X = 5) = \dfrac{3}{32}$.

            - Si la carte tirée est le roi de cœur, $X = 7$.

                $P(X = 7) = \dfrac{1}{32}$.

            - Si la carte tirée n'est ni un cœur, ni un roi, $X = –1$.
            
                $P(X = –1) = \dfrac{21}{32}$.

            La loi de probabilité de $X$ est :

            |$x_i$|$–1$|$2$|$5$|$7$|
            |:-:|:-:|:-:|:-:|:-:|
            |$P(X=x_i)$|$\dfrac{21}{32}$|$\dfrac{7}{32}$|$\dfrac{3}{32}$|$\dfrac{1}{32}$|

    2. 

        === "Question"
        
            Calculer l’espérance de $X$ et interpréter le résultat.

        === "Réponse"

            $E(X)=(-1)\times \dfrac{21}{32} +2\times \dfrac{7}{32} +5\times \dfrac{3}{32} +7\times \dfrac{1}{32} = \dfrac{15}{32}$



            L'espérance est égale à $\dfrac{15}{32}\simeq 0,50$  signifie qu'en jouant un grand nombre de fois, on peut espérer gagner en moyenne environ 0,50 € par partie.



## II. Schéma de Bernoulli, loi binomiale

### 1) Épreuve de Bernoulli

!!! prop "Définition :"

     Une épreuve de Bernoulli de paramètre $p$ est une expérience aléatoire à deux issues que l'on peut nommer "succès" ou "échec". 

     Le paramètre $p$ est égal à la probabilité de succès.

!!! Example "Exemples :"

    1. Le jeu du pile ou face : On considère comme succès "obtenir pile" et comme échec "obtenir face". 
    
        La probabilité d’un succès est égale à  $p=\dfrac{1}{2}$.

    2. On lance un dé et on considère comme succès "obtenir un six" et comme échec "ne pas obtenir un six". 
    
        La probabilité d’un succès est égale à $p=\dfrac{1}{6}$.




### 2) Schéma de Bernoulli

!!! prop "Définition :"

     Un schéma de Bernoulli est la répétition de $n$ épreuves de Bernoulli identiques et indépendantes pour lesquelles la probabilité du succès est $p$.

!!! Example "Exemple :"

     La répétition de 10 lancers d'une pièce de monnaie est un schéma de Bernoulli de paramètres $n = 10$ et  $p = \dfrac{1}{2}$.




### 3) Loi binomiale

!!! prop "Définition :"
    
     On réalise un schéma de Bernoulli composé de $n$ épreuves de Bernoulli **identiques et indépendantes** avec pour probabilité de succès $p$.

    la loi binomiale de paramètre $n$ et $p$ est la loi de probabilité $X$ qui donne **le nombre de succès** de l'expérience.

    On note $X\sim\mathcal{B}(n;p)$



!!! Example "Exemple :"

    On a représenté dans un arbre de probabilité les issues d'une expérience suivant un schéma de Bernoulli composé de 3 épreuves de Bernoulli de paramètre $p$.

    $X$ est la variable aléatoire qui donne le nombre de succès.
    
    
    On a par exemple :

    - P(X = 3) = p3.
    En effet, en suivant les branches sur le haut de l'arbre, on arrive à 3 succès avec une probabilité de p x p x p = p3.

    - X = 2 correspond aux suites d'issues suivantes :
    (Succès ; Succès ; Échec)
    (Succès ; Échec ; Succès)
    (Échec ; Succès ; Succès)
    Donc P(X = 2) = 3 p2 (1 – p) 
    En effet, les branches qui correspondent à 2 succès et 1 échec, donne une probabilité de p x p x (1 – p) = p2 (1 – p). 
    Il y a 3 branches de ce type, soit : 3 x p2 (1 – p)


Méthode : Calculer une probabilité avec une loi binomiale à l'aide d'un arbre

  Vidéo https://youtu.be/b18_r8r4K2s  

On tire trois fois de suite avec remise une carte dans un jeu de 4 cartes qui contient une carte Némo. On considère comme succès l’événement « Obtenir la carte Némo. »
X est la variable aléatoire qui compte le nombre de succès.
Calculer P(X=2). Interpréter le résultat.

La variable aléatoire X suit la loi binomiale de paramètres n=3 et p = 1/4.
On représente dans un arbre de probabilité les issues de l’expérience composée de 3 tirages.
 

À l’issue du chemin, on comptabilise les succès et les échecs ⇡

On cherche à calculer P(X=2), on repère donc les chemins présentant deux succès (*). On en compte 3.
Chacun de ces chemins correspond au calcul de probabilité : 3/4×(1/4)^2
Et donc : P(X=2)=3×3/4×(1/4)^2
                               =3×3/4×1/16
                               =9/64
La probabilité d’obtenir deux fois la carte Némo sur 3 tirages est égale à 9/64.


### 4) Avec la calculatrice ou un tableur

Méthode : Utiliser une loi binomiale

  Vidéo https://youtu.be/7k4ZYdfWEY8 -Tuto TI

  Vidéo https://youtu.be/69IQIJ7lyww - Tuto Casio

  Vidéo https://youtu.be/clrAMXKrPV4 - Tuto HP

On lance 7 fois de suite un dé à 6 faces.
Soit X la variable aléatoire égale au nombre de fois que le dé affiche un nombre supérieur ou égal à 3.
a) Quelle est la loi suivie par X ?
b) Calculer la probabilité P(X=5).
c) Calculer la probabilité P(X≤5).
d) Calculer la probabilité P(X≥3).


a) On répète 7 fois une expérience à deux issues : {3 ; 4 ; 5 ; 6} et {1 ; 2}.
Le succès est d’obtenir {3 ; 4 ; 5 ; 6}.
La probabilité du succès sur un tirage est égale à 4/6 = 2/3.
X suit donc une loi binomiale de paramètres : n = 7 et p = 2/3.

b) Avec Texas Instruments :
Touches « 2nd » et « VAR » puis choisir « binomFdP ».
Et saisir les paramètres de l’énoncé :		binomFdP(7,2/3,5)

Avec Casio :
Touche « OPTN » puis choisir « STAT », « DIST », « BINM » et « Bpd ».
Et saisir les paramètres de l’énoncé :		BinominalePD(5,7,2/3)

Avec le tableur :
Saisir dans une cellule :				=LOI.BINOMIALE(5;7;2/3;0)

On trouve P(X=5)≈ 0,31.
La probabilité d’obtenir 5 fois un nombre supérieur ou égal à 3 est environ égale à 0,31.


c) Avec Texas Instruments :
Touches « 2nd » et « VAR » puis choisir « binomFRép ».
Et saisir les paramètres de l’énoncé :		binomFRép(7,2/3,5)

Avec Casio :
Touche « OPTN » puis choisir « STAT », « DIST », « BINM » et « Bcd ».
Et saisir les paramètres de l’énoncé :		BinominaleCD(5,7,2/3)

Avec le tableur :
Saisir dans une cellule :				=LOI.BINOMIALE(5;7;2/3;1)

On trouve P(X≤5)≈ 0,74.
La probabilité d’obtenir au plus 5 fois un nombre supérieur ou égal à 3 est environ égale à 0,74.

d) P(X≥3) = 1 – P(X≤2) 
                    ≈ 1 – 0,045 (à l’aide de la calculatrice ou du tableur)
                    ≈ 0,955.


### 5) Représentation graphique

Méthode : Établir une loi binomiale avec une calculatrice ou un tableur

  Vidéo https://youtu.be/8f-cfVFHIxg - Tuto TI

  Vidéo https://youtu.be/l9OoHVRpM8U - Tuto Casio

Soit X une variable aléatoire qui suit une loi binomiale de paramètre n = 5 et p = 0,4.
Représenter graphiquement la loi suivie par X par un diagramme en bâtons.


On commence par afficher le tableau de valeurs exprimant P(X=k) pour k entier, 0≤k≤5.

Avec Texas Instruments :
Touche « Y= » et saisir comme expliqué dans la paragraphe II.3 :
 

Afficher la table : Touches « 2nd » et « GRAPH » :
 

Avec Casio :
Dans « MENU », choisir « TABLE » ;
Saisir comme expliqué dans la paragraphe II.3 :
 

Afficher la table : Touche « TABL » :
 


Avec le tableur :
Saisir dans la cellule B1 :				=LOI.BINOMIALE(A1;5;0,4;0)
Et copier cette formule vers le bas.

 


On représente ensuite la loi binomiale par un diagramme en bâtons :
 


### 6) Espérance de la loi binomiale

Exemple :
On lance 5 fois un dé à six faces.
On considère comme succès le fait d'obtenir 5 ou 6.
On considère la variable aléatoire X donnant le nombre de succès.
On a donc : p= 2/6= 1/3 et n = 5.

Propriété : Soit la variable aléatoire X qui suit la loi binomiale de paramètre n et p.
On a :   E(X) = n x p		

Ainsi :
E(X)=5× 1/3 = 5/3 ≈1,7   
On peut espérer obtenir environ 1,7 fois un 5 ou un 6, en 5 lancers.



Méthode : Calculer l’espérance d’une loi binomiale

  Vidéo https://youtu.be/95t19fznDOU 

Un QCM comporte 8 questions. A chaque question, trois solutions sont proposées ; une seule est exacte.
Chaque bonne réponse rapporte 0,5 point.
On répond au hasard à chaque question. Quelle note peut-on espérer obtenir ?


Soit X la variable aléatoire qui compte le nombre de bonnes réponses.
X suit une loi binomiale de paramètre n=8 et  p = 1/3.
E(X)=8×1/3=8/3

On peut espérer obtenir 8/3 bonnes réponses en répondant au hasard.
On peut donc espérer obtenir 8/3 ×0,5= 4/3 ≈1,33 point en répondant au hasard.

## II. Coefficients binomiaux

### 1) Définition et propriétés

Exemple :

  Vidéo https://youtu.be/-gvlrfFdaS8 

On a représenté dans un arbre de probabilité les issues d'une expérience suivant un schéma de Bernoulli composé de 3 épreuves de Bernoulli de paramètre p.
X est la variable aléatoire qui donne le nombre de succès.


Combien existe-t-il de chemins conduisant à 2 succès parmi 3 épreuves ? On dit aussi : « Combien existe-t-il de combinaisons de 2 parmi 3 ? »
	(Succès ; Succès ; Échec)
	(Succès ; Échec ; Succès)
	(Échec ; Succès ; Succès)
Il existe donc trois combinaisons de 2 parmi 3 et on note : (■(3@2))=3.

Définition : On réalise une expérience suivant un schéma de Bernoulli de paramètre n et p.
On appelle coefficient binomiale ou combinaison de k parmi n, noté (■(n@k)), le nombre de chemins conduisant à k succès parmi n épreuves sur l'arbre représentant l'expérience.

Propriétés :	(■(n@0))=1	(■(n@n))=1	(■(n@1))=n

Démonstrations :
- Il n'y a qu'un seul chemin correspondant à 0 succès parmi n épreuves :
(Échec, Échec, … , Échec)
- Il n'y a qu'un seul chemin correspondant à n succès parmi n épreuves :
(Succès, Succès, … , Succès)
- Il n'y a n chemins correspondant à 1 succès parmi n épreuves :
(Succès, Échec, Échec, … , Échec)
(Échec, Succès, Échec, … , Échec)
(Échec, Échec, Succès, … , Échec)
…
(Échec, Échec, Échec, … , Succès)
Avec la calculatrice : 
Il est possible de vérifier les résultats à l'aide d'une calculatrice. La fonction se nomme "combinaison" ou "nCr".
Pour calculer (■(25@24)), on saisie :   25combinaison24 ou 25nCr24 suivant le modèle de calculatrice.

Avec un tableur : 
La fonction se nomme "COMBIN".
Pour calculer (■(25@24)), on saisie :   =COMBIN(25;24)


### 2) Triangle de Pascal
Propriété du triangle de Pascal :	(■(n@k))=(■(n-1@k-1))+(■(n-1@k))

Démonstration pour n = 5, k = 3 : (■(5@3))=(■(4@2))+(■(4@3))
Il y a deux types de chemins comportant 3 succès parmi 5 épreuves, (■(5@3)) :
	Ceux qui commencent par un succès : il y en a 2 parmi 4, soit (■(4@2)). 
En effet, dans l'arbre, il reste à dénombrer 2 succès parmi 4 expériences.
	Ceux qui commencent par un échec : il y en a 3 parmi 4, soit (■(4@3)).
En effet, dans l'arbre, il reste à dénombrer 3 succès parmi 4 expériences.
Ces deux types de chemins sont disjoints, donc : (■(5@3))=(■(4@2))+(■(4@3)).


Blaise Pascal (1623 ; 1662) fait la découverte d’un triangle arithmétique, appelé aujourd'hui "triangle de Pascal". Son but est d'exposer mathématiquement certaines combinaisons numériques dans les jeux de hasard et les paris. Cette méthode était déjà connue des perses mais aussi du mathématicien chinois Zhu Shi Jie (XIIe siècle).
Ci-contre, le triangle de Zu Shi Jie extrait de son ouvrage intitulé Su yuan zhian (1303).



Le tableau qui suit se complète de proche en proche comme combinaisons répondant à la propriété du triangle de Pascal.
Par exemple, ci-dessous, on a : 10 + 5 = 15

  Vidéo https://youtu.be/6JGrHD5nAoc 



			          Exemple pour (■(4@2))
k
n	0	1	2	3	4	5	6
0	1						
1	1	1					
2	1	2	1				
3	1	3	3	1			
4	1	4	(■(4@2))  =6	4	1		
5	1	5	10	10	5	1	
6	1	6	15	20	15	6	1





















                  Exemple pour (■(6@4))=(■(5@3))+(■(5@4)).


## III. Application à la loi binomiale


Méthode : Calculer les probabilités d'une loi binomiale

  Vidéo https://youtu.be/1gMq2TJwSh0 

Une urne contient 5 boules gagnantes et 7 boules perdantes. Une expérience consiste à tirer au hasard 4 fois de suite une boule et de la remettre.
On appelle X la variable aléatoire qui associe le nombre de tirages gagnants.
1) Prouver que X suit une loi binomiale.
2) Calculer la probabilité d'obtenir 3 boules gagnantes.


1) On répète 4 fois une expérience à deux issues : boules gagnantes (5 issues) ; boules perdantes (7 issues).
Le succès est « obtenir une boule gagnante ».
La probabilité du succès sur un tirage est égale à 5/12.
Les paramètres de la loi binomiale sont donc : n = 4 et p = 5/12.



2) 
Propriété : Soit une variable aléatoire X qui suit la loi binomiale B(n ;p).
P(X=k)=(■(n@k)) p^k (1-p)^(n-k)

      P(X=3)=(■(4@3)) (5/12)^3 (1-5/12)^(4-3)
                         =(■(4@3)) (5/12)^3 (7/12)^(4-3)
                        =(■(4@3)) (5/12)^3×7/12
                        =(■(4@3))×125/1728×7/12
                        =(■(4@3))×875/20736

On détermine la valeur de la combinaison (■(4@3)) à l'aide du triangle de Pascal.
   k
n	0	1	2	3	4
0	1				
1	1	1			
2	1	2	1		
3	1	3	3	1	
4	1	4	6	4	1











On a donc (■(4@3))=4, et donc :
P(X=3)=4×875/20736=875/5184≈0,17.


La loi binomiale avec la calculatrice :
  Vidéo https://youtu.be/ZIy1AWJu8tg (avec TI)
  Vidéo https://youtu.be/TO1ym3lJCQo (avec Casio)
  Vidéo https://youtu.be/RZsNHNhWlsg (avec HP)









