# Terminale Spécialité Mathématiques

[Loi Binomiale](Loi_Binomiale/binomiale.md){ .md-button }

[Fonctions trigonométriques](Fct_trigo/Fct_trigo.md){ .md-button }

[Loi des grands nombres](Valeatoires/Ldgn.md){ .md-button }

[Integration](Integration/integration.md){ .md-button }