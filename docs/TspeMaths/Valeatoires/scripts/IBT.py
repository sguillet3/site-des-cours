from random import random
from math import *

def simulation_X()->int:
    '''
    simule un lancer de loi de probabilité B(20,0.1)
    '''
    succes=0
    for experience in range(20):
        if random()<0.1 :
            succes=succes+1
    return succes

def proba(n:int)->float:
    '''
    simulation d'un echantillon de taille n de la loi X
    '''
    echantillon = [simulation_X() for i in range(n)]
    compteur=0
    delta=2*sqrt(1.8) # ecart-type : delta=2 sigma
    for experience in echantillon:
        if abs(experience-2)>=delta:
            compteur=compteur+1
    return compteur/n
