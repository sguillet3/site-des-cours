import random as rd
import math

def simulMn(n):
    S=[rd.randint(1,5) for i in range(n)]
    Mn=sum(S)/n 
    return Mn

def echantMn(n):
    echant=[simulMn(n) for i in range(500)]
    c=0
    d=math.sqrt(2/n)
    for e in echant :
        if abs(e-3)>=d:
            c=c+1
    return c/500

