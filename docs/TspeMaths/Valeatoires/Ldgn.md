# CONCENTRATION, LOI DES GRANDS NOMBRES

## I. Moyenne d’un échantillon 

###	1) Définition

!!! prop "Rappel"

	Un échantillon de taille $n$ d'une loi de probabilité est une liste de n variables aléatoires indépendantes suivant cette loi. 



!!! prop "Définition"

	Soit $(X_1,X_2,\ldots,X_n )$ un échantillon de taille $n$ de variables aléatoires indépendantes suivant une même loi.
	
	La variable aléatoire moyenne $M_n$ de l’échantillon est donnée par :
	
	$$M_n=\dfrac{1}{n}(X_1+X_2+\ldots+X_n ).$$

!!! Example "Exemple"
	On lance un dé à six faces et on considère la variable aléatoire $X$ qui prend la valeur 1 si le dé s’arrête sur un chiffre pair et la valeur 0 sinon.

	$X$ suit donc une loi de Bernoulli de paramètre $\dfrac{1}{2}$.

	On répète deux fois de suite cette expérience. On considère alors l’échantillon $(X_1,X_2 )$ de taille 2 de variables aléatoires $X_1$ et $X_2$  suivant la même loi que $X$.

	Il est ainsi possible d’évaluer le résultat d’une telle expérience en étudiant la variable aléatoire moyenne de $X_1$ et $X_2$.

	On appelle $M_2$ la variable aléatoire moyenne de l’échantillon $(X_1,X_2 )$.

	Alors $M_2$ peut prendre les valeurs suivantes :

	|Valeur de $X_1$|Probabilité de $X_1$|Valeur de $X_2$|Probabilité de $X_2$|Probabilité de $(X_1,X_2 )$|Valeur de $M_2$|Probabilité de M_2|
	|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
	|$0$|$\dfrac{1}{2}$|$0$|$\dfrac{1}{2}$|$\dfrac{1}{2}\times\dfrac{1}{2}$=$\dfrac{1}{4}$|$\dfrac{0+0}{2}=0$|$\dfrac{1}{4}$|
	|$0$|$\dfrac{1}{2}$|$1$|$\dfrac{1}{2}$|$\dfrac{1}{2}\times\dfrac{1}{2}$=$\dfrac{1}{4}$|$\dfrac{0+1}{2}=\dfrac{1}{2}$|$\dfrac{1}{4}$|
	|$1$|$\dfrac{1}{2}$|$0$|$\dfrac{1}{2}$|$\dfrac{1}{2}\times\dfrac{1}{2}$=$\dfrac{1}{4}$|$\dfrac{1+0}{2}=\dfrac{1}{2}$|$\dfrac{1}{4}$|
	|$1$|$\dfrac{1}{2}$|$1$|$\dfrac{1}{2}$|$\dfrac{1}{2}\times\dfrac{1}{2}$=$\dfrac{1}{4}$|$\dfrac{1+1}{2}=1$|$\dfrac{1}{4}$|

	Et on a ainsi la loi de probabilité de $M_2$ :

	|$k$|$0$|$\dfrac{1}{2}$|$1$|
	|:-:|:-:|:-:|:-:|
	|$P(M_2=k)$|$\dfrac{1}{4}$|$\dfrac{1}{4}+\dfrac{1}{4}=\dfrac{1}{2}$|$\dfrac{1}{4}$|


###	2) Propriétés

!!! prop "Propriété :"
	 Soit une variable aléatoire $X$ et soit un échantillon $(X_1,X_2,…,X_n )$ de taille $n$ de variables aléatoires indépendantes suivant la même loi que $X$.
	 
	- $E(M_n )=E(X)$
	- $V(M_n )=\dfrac{1}{n} V(X)$
	- $\sigma (M_n )=\dfrac{1}{\sqrt{n}} \sigma (X)$


!!! Note "Méthode : Calculer l’espérance, la variance et l’écart-type d’une variable aléatoire moyenne"

	=== "Enoncé"	

		On considère la variable aléatoire $X$ qui prend, de façon équiprobable, les valeurs $–4$, $0$, $1$, $3$ et $6$. 
		
		$M_{50}$ est la variable aléatoire moyenne d’un échantillon de taille 50 de la loi de $X$.
		
		Calculer l’espérance, la variance et l’écart type de $M_{50}$.

	=== "Réponse"
	
		Par équiprobabilité, on établit le tableau de la loi de probabilité de X.
		
		<center>
		
		|$k$|$–4$|$0$|$1$|$3$|$6$|
		|:-:|:-:|:-:|:-:|:-:|:-:|
		|$P(X=k)$|$\dfrac{1}{5}$|$\dfrac{1}{5}$|$\dfrac{1}{5}$|$\dfrac{1}{5}$|$\dfrac{1}{5}$|
		
		</center>
		
		On a ainsi :
		
		- $E(X)=\dfrac{1}{5}\times (-4)+\dfrac{1}{5}\times 0+\dfrac{1}{5}\times 1+\dfrac{1}{5}\times 3+\dfrac{1}{5}\times 6 =1,2$
		- $V(X)=\dfrac{1}{5}\times (-4-1,2)^2+\dfrac{1}{5}\times (0-1,2)^2+\dfrac{1}{5}\times (1-1,2)^2+\dfrac{1}{5}\times (3-1,2)^2+\dfrac{1}{5}\times (6-1,2)^2=10,96$
		- $\sigma (X)=\sqrt{10,96}\simeq 3,31$

		On en déduit : 
		
		- $E(M_{50} )=E(X)=1,2$
		- $V(M_{50} )=\dfrac{1}{50} V(X)=\dfrac{10,96}{50}=0,2192$
		- $\sigma (M_{50} )=\dfrac{1}{\sqrt{50}} \sigma (X)\simeq \dfrac{3,31}{\sqrt{50}}\simeq 0,468$

	=== "Vidéo"
	
		<div class="youtube-player" data-id="o67OOavrbHQ"></div>

## II. Inégalité de Bienaymé-Tchebychev 

!!! prop "Propriété :"

	Soit $X$ une variable aléatoire d'esperance $E(X)=\mu$ et de variance $V(X)=V$.

	Pour tout réel strictement positif $\delta$, on a :

	$$P(|X-\mu|\geqslant \delta) \leqslant \dfrac{V}{\delta^2}$$ 


!!! Example "Méthode : Appliquer l’inégalité de Bienaymé-Tchebychev"

	??? "Vidéo" 
	
		<div class="youtube-player" data-id="4XMvq1FnYwU"></div>
		
		

	Soit une variable aléatoire $X$ qui suit la loi binomiale de paramètres $n=20$ et $p=0,1$.
	
	1. 
	
		=== "Question"
		
			Appliquer l’inégalité de Bienaymé-Tchebychev avec $\delta =2\sigma(X)$. Interpréter.
	
		=== "Réponse"
		
			- $E(X)=20\times 0,1=2$
			- $V(X)=20\times 0,1\times 0,9=1,8$
			- $\sigma(X)=\sqrt{1,8}$

			Ainsi, on obtient : 

			$P(|X-E(X)|\geqslant 2\sigma(X))\leqslant\dfrac{V(X)}{(2\sigma(X))^2}$

			Soit $P(|X-2|\geqslant 2\sqrt{1,8})\leqslant 0,25$

			La probabilité que l’écart de $X$ à $E(X)$ soit supérieur à $2\sigma(X)$ est majorée par 0,25. 
			
	2. 
	
		=== "Question"
		
			Recommencer avec $\delta =3 \sigma(X)$, puis $\delta =4 \sigma(X)$. Que constate-t-on ?
			
		=== "Réponse"

			- pour $\delta =3 \sigma(X)$ :  

				$P(|X-E(X)|\geqslant 3\sigma(X))\leqslant \dfrac{V(X)}{(3\sigma(X))^2}$

				Soit $P(|X-2|\geqslant 3\sqrt{1,8})\leqslant \dfrac{1}{9}$

			- pour $\delta =4 \sigma(X)$ :  

				$P(|X-E(X)|\geqslant 4\sigma(X))\leqslant \dfrac{V(X)}{(4\sigma(X))^2}$
				
				Soit $P(|X-2|\geqslant 4\sqrt{1,8})\leqslant 0,0625$

			- On peut en déduire que les écarts de $X$ à $E(X)$ de quelques $\sigma$ deviennent improbables.	
	3. 

		1. 
		
			=== "Question"
			
				Simuler $N$ valeurs de la variable aléatoire $X$ par une fonction en Python dans le but d’estimer la probabilité $P(|X-2| ≥2\sigma(X))$.
				
				On testera le programme pour différentes valeurs de $N$.

			=== "Réponse"
			
				{{ basthon('scripts/IBT.py', 800) }}
				
		2. 
		
			=== "Question"
			
				Au regard des résultats obtenus par le programme, peut-on penser que l’inégalité de Bienaymé-Tchebychev a un caractère optimal ?

			=== "Réponse"
			
				On constate qu’un écart à $E(X)$ supérieur à $2\sigma(X)$ est de probabilité souvent inférieure $0,05$ alors que l’inégalité de Bienaymé-Tchebychev nous donne pour cette même probabilité une majoration par $0,25$. 
				
				L’inégalité est donc loin d’être optimale.




## III. Inégalité de concentration 

!!! prop "Propriété : Inégalité de concentration"

	Soit la variable aléatoire moyenne $M_n$ d’un échantillon de taille $n$ de la variable aléatoire $X$. 	

	Pour tout réel strictement positif $\delta$, on a :
	
	$$p(|M_n-E(X)|\geqslant\delta)\leqslant\dfrac{V(X)}{n \delta^2}$$

!!! Example "Exemple"

	On lance $n$ fois un dé équilibré à 8 faces et on nomme $X_i$ la variable aléatoire donnant le résultat du i-ème lancer. 
	
	On admet que $E(X_i) = 4,5$ et $V (X_i ) = 5,25$ pour tout entier $i$ entre 1 et $n$.

	Les lancers étant indépendants, $(X_1 ; X_2 ; \ldots ; X_n)$ est un échantillon de variables aléatoires d’espérance$\mu = 4,5$, de variance $V = 5,25$.
	
	Soit $M_n$ la moyenne de cet échantillon. 

	D’après l’inégalité de concentration pour $n = 100$ et $\delta = 0,5$, on a $p(|M_{100} – 4,5| \geqslant 0,5) \leqslant \dfrac{5,25}{100\times 0,5^2}$
	
	soit $p(|M_{100} – 4,5| \geqslant 0,5) \leqslant 0,21$

	donc la probabilité que l’écart entre $M_{100}$ (la moyenne des 100 premiers résultats) et $4,5$ soit supérieur ou égal $0,5$, est inférieure ou égale à 0,21.

!!! Example "Méthode : Appliquer l’inégalité de concentration pour déterminer la taille d’un échantillon"

	=== "Question"
	
		Soit une variable aléatoire $X$ qui suit la loi de Bernoulli de paramètre $0,2$.

		On considère un échantillon de $n$ variables aléatoires suivant la loi de $X$.

		On appelle $M_n$ la variable aléatoire moyenne associée à cet échantillon.

		Déterminer la taille $n$ de l’échantillon tel que la probabilité que la moyenne $M_n$ appartienne à l’intervalle $]0,03 ;0,37[$ soit supérieure à $0,95$.

	=== "Réponse"

		On cherche à calculer $n$ tel que $P(0,03<M_n<0,37)\geqslant 0,95$

		Dans l’idée d’appliquer l’inégalité de concentration, on fait apparaitre l’espérance de $X$ dans l’inégalité.

		Or, $E(X)=p=0,2$

		Ainsi, on cherche $n$ tel que : $P(0,03-0,2<M_n-0,2<0,37-0,2)\geqslant 0,95$
		
		Soit : $P(-0,17<M_n-0,2<0,17)\geqslant 0,95$
		
		Soit encore : $P(|M_n-0,2|<0,17)\geqslant 0,95$

		Et donc, en considérant l’évènement contraire :
		
		$P(|M_n-0,2|\geqslant 0,17)\leqslant 0,05$

		En prenant $\delta =0,17$ dans l’inégalité de concentration, on a :
		
		$P(|M_n-E(X)|\geqslant 0.17)\leqslant 0,05$, avec $\dfrac{V(X)}{n δ^2}  =0,05$.

		Or, $V(X)=p(1-p)=0,2\times 0,8=0,16$

		On cherche donc un entier $n$ tel que : $\dfrac{0,16}{n\times 0,17^2}\leqslant 0,05$

		Et donc : $n\geqslant \dfrac{0,16}{0,05\times 0,17^2}\simeq 110,7$

		Pour $n\geqslant 111$, la probabilité que la moyenne $M_n$ appartienne à l’intervalle $]0,03 ;0,37[$ est supérieure à 0,95.

	=== "Vidéo"
	
		 <div class="youtube-player" data-id="7Nk9U-zwWOA"></div>

## IV. Loi des grands nombres 

!!! prop "Propriété : loi (faible) des grands nombres"

	Soit la variable aléatoire moyenne $M_n$ d’un échantillon de taille $n$ de la variable aléatoire $X$. 
	
	Pour tout réel strictement positif $\delta$ fixé, $\lim\limits_{n\rightarrow +\infty} p(|M_n-E(X)|\geqslant \delta)=0$

!!! Example "Exemple"

	On reprend l’exemple précédent et on considère $\delta = 0,1$.

	D’après la loi des grands nombres, $p(|M_n – 4,5| \geqslant 0,1)$, que l’on peut également écrire $p(M_n \not\in ]4,4 ; 4,6[)$,
	tend vers 0 lorsque la taille de l’échantillon tend vers $+\infty$.

	On en déduit que $p(M_n \in ]4,4 ; 4,6[)$ tend vers $1$ lorsque la taille de l’échantillon tend vers $+\infty$. 
	
	Autrement dit, si l’on fait un nombre suffisamment grand de lancers, on peut rendre l’événement « la moyenne
	de l’échantillon est dans $]4,4 ; 4,6[$ » aussi probable qu’on le souhaite en prenant $n$ suffisamment grand.


!!! warning "Remarque"
	Dans l’exemple, on aurait pu prendre $\delta = 0,01$ ou $0,001$, etc. : la loi des grands nombres illustre le fait que la moyenne de l’échantillon se rapproche de l’espérance des variables aléatoires quand la taille de l’échantillon « devient grande ».

