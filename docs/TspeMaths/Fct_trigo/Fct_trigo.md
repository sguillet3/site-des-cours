---
title: Fonctions trigonometriques
---


<center>

#    Fonctions trigonométriques

</center>



## 1. Définitions

Dans le plan muni d'un repère orthonormé $\left(O ;\, \overrightarrow{i};\,\overrightarrow{j} \right)$ et orienté dans le sens direct, soit le cercle trigonométrique de centre $O$.

Pour tout nombre réel $x$, on définit le point $N$ de la droite orientée d’abscisse $x$.

À ce point, on fait correspondre un point $M$ sur le cercle trigonométrique par enroulement de la droite.

On appelle H et K les pieds respectifs des perpendiculaires à l’axe des abscisses et à l’axe des ordonnées passant par M.



??? "Animation : Lancer ou relancer l'animation à l'aide du bouton"

	<center>
	<iframe scrolling="no" title="def_cercle" src="https://www.geogebra.org/material/iframe/id/uxznm9cp/width/500/height/500/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="500px" height="500px" style="border:0px;"> </iframe>
	</center>

!!! prop "Définition"

	- Le cosinus du nombre réel $x$ est l'abscisse de $M$ et on le note $\cos x$.
	- Le sinus du nombre réel $x$ est l'ordonnée de $M$ et on le note $\sin x$.



!!! prop "Propriété :"
    Pour tout nombre réel $x$, on a :
	
    - $-1\leqslant \cos ⁡x \leqslant 1$
    - $-1\leqslant \sin ⁡x \leqslant 1$
    - $\cos^2 x + \sin^2 x = 1$


</br></br>

------------------------------------------------------------------------

**Valeurs remarquables des sinus et cosinus :**

<center>

|$x$|0|$\dfrac{\pi}{6}$|$\dfrac{\pi}{4}$|$\dfrac{\pi}{3}$|$\dfrac{\pi}{2}$|$\pi$|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|$\cos ⁡x$|1|$\dfrac{\sqrt{3}}{2}$|$\dfrac{\sqrt{2}}{2}$|$\dfrac{1}{2}$|0|$-1$|
|$\sin ⁡x$|0|$\dfrac{1}{2}$|$\dfrac{\sqrt{2}}{2}$|$\dfrac{\sqrt{3}}{2}$|1|0|

</center>

## 2. Fonctions trigonométriques

### 1. Définition


!!! prop "Définition"

	- La fonction **cosinus** est définie sur $\mathbb{R}$ par :
	
		$\begin{aligned}
		\cos : \mathbb{R}&\longrightarrow [-1;1]\\
		x&\longmapsto \cos x\end{aligned}$
	
	
	- La fonction **sinus** est définie sur $\mathbb{R}$ par :
	
		$\begin{aligned}
		\sin : \mathbb{R}&\longrightarrow [-1;1]\\
		x&\longmapsto \sin x\end{aligned}$
	
### 2. Périodicité


!!! prop "Propriétés"
	Les fonction et **sinus** sont périodiques de période $2\pi$.


	Autrement dit, $\forall k\in\mathbb{Z}$ (pour tout entier relatif $k$)

	- $\cos ⁡x=\cos⁡(x+2k\pi)$
	- $\sin ⁡x=\sin⁡(x+2k\pi)$


!!! Abstract "Demonstration"

	Aux points de la droite orientée d'abscisses $x$ et $x+2k\pi$ on fait correspondre le même point du cercle trigonométrique.


!!! Note "**Conséquences :**"

	Pour tracer la courbe représentative de la fonction cosinus ou de la fonction sinus, il suffit de la tracer sur un intervalle de longueur $2\pi$ et de la compléter par translation.

!!! Example "Méthode"

	1. 
		=== "Enoncé"
	
			Résoudre dans $\mathbb{R}$, l'équation : $\cos^2 ⁡x= \dfrac{1}{2}$.
			
		=== "Corrigé"
			
			$\cos^2 ⁡x=  \dfrac{1}{2}$
			
			$\cos^2 ⁡x- \dfrac{1}{2} =0$
			
			$\left(\cos ⁡x-\dfrac{1}{\sqrt{2}}\right)\left(\cos⁡ x+\dfrac{1}{\sqrt{2}}\right)=0$
			
			Soit : $\left(\cos ⁡x-\dfrac{\sqrt{2}}{2}\right)\left(\cos⁡ x+\dfrac{\sqrt{2}}{2}\right)=0$
			
			donc 
			
			$\begin{aligned}
			\cos x&=\dfrac{\sqrt{2}}{2}&&\text{ ou }&\cos x&=-\dfrac{\sqrt{2}}{2}
			\end{aligned}$

			Soit :
			
			- pour $\cos x=\dfrac{\sqrt{2}}{2}$ : $S_1=\left\{\dfrac{\pi}{4}+2k \pi,\quad  k\in\mathbb{Z}\right\}\cup\left\{-\dfrac{\pi}{4}+2k \pi,\quad  k\in\mathbb{Z}\right\}$
			- pour $\cos x=-\dfrac{\sqrt{2}}{2}$ : $S_2=\left\{\dfrac{3\pi}{4}+2k \pi,\quad  k\in\mathbb{Z}\right\}\cup\left\{-\dfrac{3\pi}{4}+2k \pi,\quad  k\in\mathbb{Z}\right\}$
			
			Finalement :

			$S=S_1\cup S_2=\left\{\dfrac{\pi}{4}+k\dfrac{\pi}{2},\quad k\in\mathbb{Z}\right\}$
		
		=== "Vidéo"
		
			<div class="youtube-player" data-id="PcgvyxU5FCc"></div>
	
	2. 
		
		=== "Enoncé"
			
			Résoudre dans $]-\pi ; \pi]$, l'inéquation : $\cos ⁡x\leqslant \dfrac{\sqrt{3}}{2}$.
			
		=== "Corrigé"
		
			- On commence par résoudre l'équation $\cos ⁡x= \dfrac{\sqrt{3}}{2}$ dans $]-\pi ; \pi]$.
			
				Soit : $x=\dfrac{\pi}{6}$ ou $x=-\dfrac{\pi}{6}$.

			- On utilise le cercle trigonométrique pour conclure sur les solutions de l'inéquation $\cos ⁡x\leqslant \dfrac{\sqrt{3}}{2}$.
			
				Cela correspond à la zone du cercle située à gauche de la droite passant par les points du cercle correspondant
				aux valeurs $\dfrac{\pi}{6}$ et $-\dfrac{\pi}{6}$.
				
			Donc :
			
			$S=\left]-\pi ; -\dfrac{\pi}{6}\right]\cup\left[\dfrac{\pi}{6};\pi\right]$
			
		=== "Vidéo"
		
			<div class="youtube-player" data-id="raU77Qb_-Iw"></div>	
			
		
### 3. Parité


!!! prop "Définition"
	- Une fonction $f$ est **paire** lorsque pour tout réel $x$ de son ensemble de définition $D_f$:
	
		
		+ $-x$ appartient à $D_f$
		
		et
		
		+ $f(-x)=f(x)$.
		
		
	- Une fonction $f$ est **impaire** lorsque pour tout réel $x$ de son ensemble de définition $D_f$:
	
		+ $-x$ appartient à $D_f$
		
		et
		
		+ $f(-x)=- f(x)$.</td>
		



!!! prop "Propriété"

	- **La fonction cosinus est paire**; 
	
	- **la fonction sinus est impaire**.
	
	Autrement dit, pour tout nombre réel $x$ ($\forall x\in\mathbb{R}$) :
	
	- $\cos⁡(-x)=\cos ⁡x$
	- $\sin⁡(-x)=-\sin ⁡x$

	??? warning "Graphique"
	
		![cos_sin](images/parite.png){width=250 }
	
!!! Note "**Conséquences :**"

	- Dans un repère orthogonal, la courbe représentative de la fonction cosinus est symétrique par rapport à l'axe des ordonnées.
	- Dans un repère orthogonal, la courbe représentative de la fonction sinus est symétrique par rapport à l'origine.

!!! Example "Méthode"
	
	=== "Question"
	
		Démontrer que la fonction $f$ définie sur $\mathbb{R}$ par $f(x)=sin⁡x-sin⁡(2x)$ est impaire.
	
	=== "Réponse"
		
		Pour tout réel $x$, on a : 
		
		$f(-x)=\sin⁡(-x)-\sin⁡(-2x)=-\sin⁡x+\sin⁡(2x)=-f(x)$.
		
		La fonction $f$ est donc impaire. 
		
		Sa représentation graphique est symétrique par rapport à l'origine du repère.
		
	=== "Vidéo"
		
		<div class="youtube-player" data-id="hrbgxnCZW_I"></div>


## 3. Dérivabilité et variations

### 1. Dérivabilité

!!! prop "Théorème"
	Les fonctions cosinus et sinus sont dérivables sur $\mathbb{R}$ et on a : 
	
	$(\cos(x))' = -\sin(x)$  et $(\sin(x))' = \cos(x)$


!!! Abstract "Démonstration"

	La démonstration sera disponible ultérieurement.

<!---	 === "Etape 1"
	
[//]: #		??? "figure"
			
<>			<iframe scrolling="no" title="derivation" src="https://www.geogebra.org/material/iframe/id/d5apxwg4/width/602/height/533/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="602px" height="533px" style="border:0px;"> </iframe>
-->
			
!!! Warning "Remarque :"

	- $(\cos(x))'$ se note également $\cos'(x)$
	- Les fonctions $\sin$ et $\cos$ sont solutions de l'équation différentielle $\quad y''+y=0$

### 2. Variations

#### 1. Fonction cosinus

<center>

![cos_sin](images/tabvar_cos.png)
</center>

#### 2. Fonction sinus

<center>

![cos_sin](images/tabvar_sin.png)
</center>

### 3. Représentations graphiques

#### 1. Fonction cosinus

<center>

![cos_sin](images/cosinus.png){width=60%}
</center>

#### 2. Fonction sinus

<center>

![cos_sin](images/sinus.png){width=60%}
</center>
	
#### 3. Etude d'une fonction

!!! Example "Méthode : Etudier une fonction trigonométrique"

	On considère la fonction $f$ définie sur $\mathbb{R}$ par $f(x)=\cos⁡(2x)-\cfrac{1}{2}$.
	
	1. 
		=== "Question"
	
			Etudier la parité de $f$.
	
		=== "Réponse"
		
			Pour tout $x$ de $\mathbb{R}$, on a : 
			
			$f(-x)=\cos⁡(-2x)- \dfrac{1}{2} =\cos⁡(2x)- \dfrac{1}{2}=f(x)$
			
			La fonction $f$ est donc paire. Dans un repère orthogonal, sa représentation graphique est donc symétrique par rapport à l'axe des ordonnées.
			
	2. 
		
		=== "Question"
		
			Démontrer que la fonction $f$ est périodique de période $\pi$.
			
		=== "Réponse"
			Pour tout $x$ de $\mathbb{R}$, on a : 
			
			$f(x+\pi)=\cos⁡(2(x+\pi))- \dfrac{1}{2} =\cos⁡(2x+2\pi)- \dfrac{1}{2} =\cos⁡(2x)- \dfrac{1}{2} =f(x)$
	
			On en déduit que la fonction $f$ est périodique de période $\pi$.
			
	3. 
	
		=== "Question"
		
			Etudier les variations de $f$ sur $\left[0 ; \cfrac{\pi}{2}\right]$.
	
		=== "Réponse"
		
			- Soit $u(x)=2x$, par composition :
			
				$(\cos(u))'=u'\times \cos'(u)=u'\times (-\sin(u))=-2\sin(2x)$
			
				donc
			
				$f'(x)=-2\sin(2x)$
			
			- Pour $x\in \left[0 ; \cfrac{\pi}{2}\right]$, $2x \in [0;\pi]$ donc 
				
				$\sin(2x)\geqslant 0$
				
				donc
				
				$f'(x)\leqslant 0$ sur $\left[0 ; \cfrac{\pi}{2}\right]$
				
				donc
				
				$f$ est décroissante sur $\left[0 ; \cfrac{\pi}{2}\right]$


	4. 
	
		=== "Question"
		
			Représenter graphiquement la fonction f sur $\left[0 ; \cfrac{\pi}{2}\right]$ et prolonger de part et d'autre la représentation par symétrie et par translation.
	
		=== "Réponse"
			
			<iframe scrolling="no" title="trace" src="https://www.geogebra.org/material/iframe/id/bvtxzq5b/width/800/height/400/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="800px" height="400px" style="border:0px;"> </iframe>
			
<!--=== "Tracé sur $\left[0 ; \cfrac{\pi}{2}\right]$"
				On commence par tracer la courbe sur l'intervalle $\left[0 ; \dfrac{\pi}{2}\right]$.
				
				D'où :
				
				![essai](./images/trace.png "essai"){width=600}
				
			=== "Parité"
				La fonction $f$ est paire, donc sa courbe représentative est symétrique par rapport à l'axe des ordonnées. 
				
				On peut ainsi prolonger la courbe par symétrie axiale sur l'intervalle $\left[-\dfrac{\pi}{2} ; 0\right]$.
				
				![essai](./images/parite.gif "essai") 
				
			=== "Périodicité"
				La fonction $f$ est périodique de période , on peut ainsi prolonger la courbe en translatant horizontalement la portion de courbe déjà tracée. 
				
				En effet, la portion déjà tracée se trouve sur l'intervalle $\left[-\dfrac{\pi}{2} ; \dfrac{\pi}{2}\right]$ de longueur $\pi$.
				![essai](./images/periode.gif "essai")
-->
				

			