

<center>
# Intégration
</center>

## I - Intégrale d’une fonction continue et positive



![integrale](images/integrale.png){align=left}



!!! prop "Définition"
	
	Soit $f$ une fonction continue et positive sur un intervalle $[a ;b]$.
	
	On appelle intégrale de $f$  sur $[a ;b]$ l'aire, exprimée en u.a. (unités d’aire), de la surface délimitée par la courbe représentative de la fonction $f$, l'axe des abscisses et les droites d'équations $x=a$ et $x=b$.
	
	L'intégrale de la fonction $f$ sur $[a ;b]$ se note $\int_a^b f(x)\,dx$
	
	Et on lit « intégrale de $a$ à $b$ de $f(x)\,dx$ ».
	
##

!!! Abstract "Remarques :"
	- $a$ et $b$ sont appelés les bornes d'intégration.
	- $x$ est la variable. Elle peut être remplacée par toute autre lettre qui n'intervient pas par ailleurs.
	
		Ainsi on peut écrire : $\int_a^b f(x)\,dx=\int_a^b f(t)\,dt$.
	  
		"$dx$" ou "$dt$" nous permet de reconnaître la variable d'intégration.
	
	
!!! Note "Exemple :"

	La fonction $f$ est définie sur $\mathbb{R}$ par $f(x)=4-x^2$. 
	On s’intéresse à $\int_0^2 f(x)\,dx$.
 
	=== "Approche géométrique" 
		Approche de la valeur de l’intégrale avec des rectangles
	
		![integrale](images/int1.png){align=left}
		![integrale](images/int2.png){align=left}
		
	=== "A la calculatrice"  

		![integrale](images/int_calc.png){align=left}










!!! Example "Exercice : "

	

	On a représenté la fonction $f$ définie sur $\mathcal{R}$ par  $f(x)=x+2$.

	![integrale](images/int3.png){width=300px}

	On note $S(x)$ l’aire, en unités d’aire, du domaine coloriée.

	On s’intéresse donc à la fonction $S$ telle que $S(x)=\int_0^x f(t)dt$.

	1. 
		=== "Question"

			En utilisant la formule donnant l’aire d’un trapèze, exprimer l’aire $S(x)$ en fonction de $x$.

		=== "Réponse"

			L'aire d'un trapèze est donné par la formule :

			$\dfrac{(petite\;base+grande\;base)\times hauteur}{2}$

			donc : $S(x)=\dfrac{2+(f(x))\times x}{2}$

			soit $S(x)=\dfrac{(2+x+2)\times x}{2}=\dfrac{(x+4)\times x}{2}=\dfrac{x^2+4x}{2}=\dfrac{x^2}{2}+2x$
	2. 
		
		=== "Question"

			Montrer que la fonction $S$ est la primitive de $f$ qui s’annule en 0.

		=== "Réponse"

			- $S'(x)=\dfrac{2x}{2}+2=x+2=f(x)$

				donc $S$ est bien une primitive de $f$.
			
			- $S(0)=0$

				donc $S$ est bien la primitive de $f$ qui s'annule en 0.

		

		


 
## II - Intégrale et primitives

### 1 - Fonction définie par une intégrale

!!! prop "Théorème :"

	Soit $f$ une fonction continue et positive sur un intervalle $[a ;b]$.

	La fonction $F_a$ définie sur $[a ;b]$ par $F_a (x)=\displaystyle \int_a^x f(t) dt$ est la primitive de $f$ qui s’annule en $a$.

	??? prop "Représentation graphique"

		![integrale](images/int4.png){width=400px}

??? Warning "Démonstration dans le cas où f est strictement croissante :"

	Cela revient à démontrer que :  $\lim\limits_{h\rightarrow 0}⁡ \dfrac{F_a (x+h)-F_a (x)}{h}=f(x)$.

	- Soit \(x\in [a;b]\) et $h>0$ tel que $x+h\in [a;b]$

		$F_a (x+h)-F_a(x)=\displaystyle\int_a^{x+h}f(x) dx-\int_a^x f(x)  dx=\int_x^{x+h} f(x)  dx$ 

		![integrale](images/int5.png){width=300px}

		$Aire(ABFE)=h\times f(x)$ et  $Aire(ABHG)=h\times f(x+h)$.

		Comme $f$ est croissante sur $[a ;b]$, on a :

		$$h\times f(x) < F_a (x+h)-F_a (x)<h\times f(x+h)$$

		Puisque $h>0$, on a :   
		
		$$f(x)<\dfrac{F_a (x+h)-F_a (x)}{h}<f(x+h)$$

		$\displaystyle\lim_{h\rightarrow 0^+}f(x)=f(x)$  et  $\displaystyle\lim_{h\rightarrow 0^+}f(x+h)=f(x)$.

		D'après le théorème des gendarmes :  
		
		$\displaystyle\lim_{h\rightarrow 0^+}\dfrac{F_a (x+h)-F_a (x)}{h}=f(x)$ 

	- Un même raisonnement permet d’obtenir : 
	
		$\displaystyle\lim_{h\rightarrow 0^-}\dfrac{F_a (x+h)-F_a (x)}{h}=f(x)$

	Les limites à gauche et à droite existe et sont identiques donc  
	
	$\displaystyle\lim_{h\rightarrow 0}\dfrac{F_a (x+h)-F_a (x)}{h}=f(x)$.

	Or par définition,  $F_a'(x)=\lim_{h\rightarrow 0}\dfrac{F_a (x+h)-F_a (x)}{h}$  donc $F_a' (x)=f(x)$.

	$F_a$ est donc une primitive de $f$. Par ailleurs, $F_a$ s’annule en $a$, car $F_a (a)=\displaystyle\int_a^a f(t)dt=0$.

### 2- Calcul d’intégrales

!!! prop "Théorème :"

	Soit $f$ une fonction continue et positive sur un intervalle $[a ;b]$.
	
	Si $F$ est une primitive de $f$ alors  $\displaystyle\int_a^b f(x) dx=\left[ F(x)\right]_a^b=F(b)-F(a)$.

??? Warning "Démonstration :"
	$F$ est une primitive quelconque de $f$ et  $F_a (x)=\displaystyle\int_a^x f(t) dt$ est la primitive de $f$ qui s’annule en $a$. 
	
	Il existe un réel $k$ tel que $F=F_a+k$.

	$F(b)-F(a)=F_a (b)+k-F_a (a)-k=F_a (b)=\displaystyle\int_a^b f(t) dt$

!!! Example "Exemples :"

	1. 
		=== "Question"
			Calculer $A=\displaystyle\int_1^4 \dfrac{3}{x^2}dx$
		
		=== "Réponse"

			$\begin{array}{rl}
			A=\displaystyle\int_1^4 \dfrac{3}{x^2}dx&=\left[-\dfrac{3}{x}\right]_1^4\\
			&=-\dfrac{3}{4}-\left(-\dfrac{3}{1}\right)\\
			&=\dfrac{9}{4}\\
			\end{array}$
	2. 

		=== "Question"
			Calculer $B=\displaystyle\int_2^5 (3x^2+4x-5)dx$

		=== "Réponse"
			$\begin{array}{rl}
			B=\displaystyle\int_2^5 (3x^2+4x-5)dx&=\left[x^3+2x^2-5x\right]_2^5\\
			&=(5^3+2\times 5^2-5\times 5)-(2^3+2\times 2^2-5\times 2)\\
			&=144\\\end{array}$	
	3. 

		=== "Question"
			Calculer $C=\displaystyle\int_{-1}^1 \text{e}^{-2x}   dx$

		=== "Réponse"
			$\begin{array}{rl}
			C=\displaystyle\int_{-1}^1 \text{e}^{-2x} dx&=\left[\dfrac{\text{e}^{-2x}}{-2}\right]_{-1}^1\\
			&=\dfrac{\text{e}^{-2\times1}}{-2}-\dfrac{\text{e}^{-2\times(-1)}}{-2}\\
			&=\dfrac{1}{2}\left(\text{e}^2-\text{e}^{-2}\right)\end{array}$
	
	4. 

		=== "Question"
			Calculer $D=\displaystyle\int_0^1 \dfrac{\text{e}^x}{\text{e}^x+3} dx$

		=== "Réponse"
			$\begin{array}{rl}
			D=\displaystyle\int_0^1 \dfrac{\text{e}^x}{\text{e}^x+3} dx&=\left[\ln \left(\text{e}^x+3\right)\right]_0^1\\
			&=\ln \left(\text{e}^1+3\right)-\ln \left(\text{e}^0+3\right)\\
			&=\ln(\text{e}+3)-\ln 4\\
			\end{array}$




### 3- Extension aux fonctions de signe quelconque

!!! prop "Définition :"

	 Soit $f$ une fonction continue sur un intervalle $[a ;b]$.

	On appelle intégrale de $f$ sur $[a ;b]$ le nombre $I=\displaystyle\int_a^b f(x) dx$ défini par :

	- si $f$ est positive sur $[a ;b]$ :  $I=Aire(E)$
	- si $f$ est négative sur $[a ;b]$ :  $I=-Aire(E)$
	
	où  $E$ est la surface délimitée par la courbe représentative de la fonction $f$, l'axe des abscisses et les droites d'équations $x=a$ et $x=b$.

	??? prop "Graphique"

		![integrale](images/int6.png){width=400px}

!!! prop "Théorème :"

	Toute fonction continue sur un intervalle admet des primitives.

!!! prop "Théorème :"

	Soit $f$ une fonction continue sur un intervalle $I$ contenant $a$ et $b$.
	
	Si $F$ est une primitive de $f$ sur $I$ alors  $\displaystyle\int_a^b f(x) dx=\left[F(x)\right]_a^b=F(b)-F(a)$.

!!! prop "Propriétés :"
	
	- $\displaystyle\int_a^a f(x) dx=0$
	- $\displaystyle\int_b^a f(x)dx= - \displaystyle\int_a^b f(x)dx$


!!! Example "Exemples :"

	1. 
		=== "Question"
			Calculer $E=\displaystyle\int_{-2}^2 x^3 dx$
			
		=== "Réponse"

			$\begin{array}{rl}
			E=\displaystyle\int_{-2}^2 x^3 dx&=\left[\dfrac{x^4}{4}\right]_{-2}^2\\
			&=\dfrac{2^4}{4}-\dfrac{(-2)^4}{4}\\
			&=0\\
			\end{array}$
	2. 
		
		=== "Question"
			Calculer $F=\displaystyle\int_0^2 (-2x+1)\text{e}^{-x^2+x} dx$

		=== "Réponse"
			$\begin{array}{rl}
			F=\displaystyle\int_0^2 (-2x+1)\text{e}^{-x^2+x} dx&=\left[\text{e}^{-x^2+x}\right]_0^2\\
			&=\text{e}^{-2^2+2}-\text{e}^{-0^2+0}\\
			&=\text{e}^{-2}-1\\
			\end{array}$

## III- Propriétés

### 1- Relation de Chasles

!!! rpop "Propriété"
	soit $f$ une fonction continue sur un intervalle $I$ et  $a$, $b$ et $c$ appartenant à $I$.

	$\displaystyle\int_a^c f(x)dx=\displaystyle\int_a^b f(x)dx+\displaystyle\int_b^c f(x)dx$

??? Warning "Preuve :"
 
	$\displaystyle\int_a^b f(x)dx+\displaystyle\int_b^c f(x)dx =F(b)-F(a)+F(c)-F(b)=F(c)-F(a)=\displaystyle\int_a^c f(x)dx$

??? "Interprétation graphique :"

	$a<b<c$ et  $f$ est positive sur $[a ;c]$

	![integrale](images/int7.png){width=400px}

### 2- Linéarité

!!! prop "Propriété"

	Soit $f$ et $g$ deux fonctions continues sur un intervalle $I$ et   $a$, $b$ deux réels appartenant à $I$.

	Soit $k\in\mathbb{R}$.
     
	- $\displaystyle\int_a^b kf(x)dx=k\displaystyle\int_a^b f(x)dx$
	- $\displaystyle\int_a^b (f(x)+g(x))dx=\displaystyle\int_a^b f(x)dx+\displaystyle\int_a^b g(x)dx$

!!! Example "Exemple :"
 	
	 Soit $f$ et $g$ sont deux fonctions continues sur l’intervalle $[-1;3]$ telles que :
	 
	 $\displaystyle\int_{-1}^3 f(x)dx=6$ et $\displaystyle\int_{-1}^3 g(x)dx=-2$.
	 
	=== "Question"
	
		Déterminer  $K=\displaystyle\int_{-1}^3 (3f(x)-g(x))dx$.

	=== "Réponse :"
	
		 $\begin{array}{rl}
		 K&=\displaystyle\int_{-1}^3 (3f(x)-g(x))dx\\
		 &=3 \displaystyle\int_{-1}^3 f(x)dx-\displaystyle\int_{-1}^3 g(x)dx\\
		&=3\times 6-(-2)=20\\
		\end{array}$

### 3 -Positivité

!!! prop "Propriété :"

	Soit $f$ est une fonction continue sur un intervalle $I$, et $a$, $b$ appartenant à $I$.

	Si $a\leqslant b$ et si pour tout $x\in [a;b]$,$f(x)\geqslant 0$ alors $\displaystyle\int_a^b f(x)dx\geqslant 0$

### 4 -Intégration des inégalités

!!! prop "Propriété :"

	Soit $f$ et $g$ deux fonctions continues sur un intervalle $I$ et  $a$, $b$ appartenant à $I$.

	Si $a\leqslant b$ et si pour tout $x\in [a ;b]$,$f(x)\leqslant g(x)$ alors  $\displaystyle\int_a^b f(x)dx\leqslant\displaystyle\int_a^b g(x)dx$
	
!!! Example "Exemple :"

	1. 
		=== "Question"
			Démontrer que pour tout $x\in [0 ; 1]$, on a : $0\leqslant \text{e}^{x^2}\leqslant \text{e}^x$.
	
		=== "Réponse"
			- $\forall x\in [0;1],\, 0\leqslant \text{e}^{x^2}$
			- $\forall x\in [0;1],\, x^2\leqslant x$ donc $\text{e}^{x^2}\leqslant \text{e}^x$
			
			Donc 

			$\forall x\in [0;1],\, 0\leqslant \text{e}^{x^2}\leqslant \text{e}^x$
	2. 

		=== "Question"
			En déduire que : $0\leqslant \displaystyle\int_0^1 \text{e}^{x^2} dx\leqslant \text{e}-1$.

		=== "Réponse"
			$\forall x\in [0;1],$

			$0\leqslant \text{e}^{x^2}\leqslant \text{e}^x$

			donc

			$\displaystyle\int_0^1 0 dx \leqslant \int_0^1\text{e}^{x^2} dx\leqslant \int_0^1\text{e}^x dx$

			$\displaystyle\left[0\right]_0^1 \leqslant \int_0^1\text{e}^{x^2} dx\leqslant \left[\text{e}^x\right]_0^1$

			Soit

			$\displaystyle 0 \leqslant \int_0^1\text{e}^{x^2} dx\leqslant \text{e}-1$








## IV- Aire entre deux courbes

!!! Example "Exemple :"

	![integrale](images/int8.png){align=right width=300px}

	 Soit les fonctions $f$ et $g$ définies par $f(x)=x^2+1$ et  $g(x)=-x^2+2x+5$.

	On admet que pour tout $x\in [-1 ;2]$, on a $f(x)\leqslant g(x)$.

	=== "Question"
		Déterminer l'aire délimitée par les courbes représentatives de $f$ et de $g$ sur l'intervalle $[-1 ;2]$.

	=== "Réponse :"
	
		 On calcule la différence entre l’aire sous la courbe représentative de $g$ et l’aire sous la courbe représentative de $f$ soit :
		
		$A=\displaystyle\int_{-1}^2 g(x) dx-\displaystyle\int_{-1}^2 f(x) dx$

		En utilisant la linéarite de l’intégrale :
		
		$\begin{array}{rl}
		A&=\displaystyle\int_{-1}^2 (g(x)-f(x)) dx\\
		&=\displaystyle\int_{-1}^2 (-2x^2+2x+4) dx \\
		&=\left[\dfrac{-2}{3} x^3+x^2+4x\right]_{-1}^2\\
		&=\dfrac{20}{3}-\left(\dfrac{-7}{3}\right)=9\end{array}$ 

!!! prop "Propriété : "

	![integrale](images/int9.png){align=right width=300px}

	Soit $f$ et $g$ sont deux fonctions continues sur un intervalle $I$ telles  que pour tout $x\in[a ;b]$, $f(x)\leqslant g(x)$ 
	
	L'aire, exprimée en unités d’aire, du domaine délimité par les courbes représentatives des fonctions $f$ et $g$ et les droites d'équations $x=a$ et $x=b$ est égale à :
	
	$\displaystyle\int_a^b (g(x)-f(x))  dx$.

## V- Valeur moyenne d’une fonction

!!! prop "Définition :"

	Soit $f$ une fonction continue sur un intervalle $[a ;b]$ avec $a\neq b$.
	
	On appelle valeur moyenne de $f$ sur $[a ;b]$ le nombre réel :

	$m=\dfrac{1}{b-a} \displaystyle\int_a^b f(x) dx$

!!! Example "Exemple :"

	Soit la fonction $f$ définie sur $\mathbb{R}$ par $f(x)=x^3-3x+3$.

	=== "Question"
    	
		Calculer la valeur moyenne de $f$ sur $[-2 ;1]$.

	=== "Réponse :"
	
		$\begin{array}{rl}
		m&=\dfrac{1}{b-a} \displaystyle\int_a^b f(x) dx\\
		&=\dfrac{1}{1-(-2)} \displaystyle\int_{-2}^1 (x^3-3x+3))dx\\
		&=\dfrac{1}{3}\times\left[\dfrac{1}{4} x^4-\dfrac{3}{2} x^2+3x\right]_{-2}^1\\
		&=\dfrac{1}{3} \left(\dfrac{7}{4}-(-8)\right)\\
		&=\dfrac{1}{3}\times\dfrac{39}{4}\\
		&=\dfrac{13}{4}\\
		\end{array}$

!!! Note "Interprétation géométrique :" 

	![integrale](images/int10.png){align=right width=300px}

	Cette valeur moyenne  est  la hauteur $h$ du rectangle de même largeur et de même aire que le domaine hachuré.


<!--### Intégration par parties-->



