# LOI BINOMIALE

## 1 Répétition d’expériences indépendantes

!!! prop "Définition"
    Plusieurs expériences sont identiques et indépendantes si :
    
    - elles ont les mêmes issues,
    - les probabilités de chacune des issues ne changent pas d’une expérience à l’autre.

!!! prop "Propriété"

    On considère une expérience aléatoire à deux issues $A$ et $B$ avec les probabilités $p(A)$ et $p(B)$.

    Si on répète l’expérience deux fois de suite de façon indépendante :

    - la probabilité d’obtenir l’issue $A$ suivie de l’issue $B$ est égale à $p(A) \times p(B)$,
    - la probabilité d’obtenir l’issue $B$ suivie de l’issue $A$ est égale à $p(B) \times p(A)$,
    - la probabilité d’obtenir deux fois l’issue $A$ est égale à $p(A)^2$ ,
    - la probabilité d’obtenir deux fois l’issue $B$ est égale à $p(B)^2$ .

!!! example "Exemples"

    === "Expérience 1"
    
        On lance un dé plusieurs fois de suite et on note à chaque fois le résultat. 
        
        On répète ainsi la même expérience (lancer un dé) et les expériences sont indépendantes l’une  l’autre (un lancer n’influence pas le résultat d’un autre lancer).

    === "Expérience 2"
    
        Une urne contient 2 boules blanches et 3 boules noires. 
        
        On tire au hasard une boule et on la remet dans l’urne. 
        
        On répète cette expérience 10 fois de suite. Ces expériences sont identiques et indépendantes.

!!! example "Méthode : Représenter la répétition d’expériences identiques et indépendantes dans un arbre"

    Vidéo : https://youtu.be/e7jH8a1cDtg

    On considère l’expérience suivante :

    - Une urne contient 3 boules blanches et 2 boules rouges.
    - On tire au hasard une boule et on la remet dans l’urne. 
    - On répète l’expérience deux fois de suite. 
    
    </br>

    1.  === "Question"  
    
            Représenter l’ensemble des issues de ces expériences dans un arbre.

        === "Réponse"
        
            On note A l’issue ”On tire une boule blanche” et B l’issue ”On tire une boule rouge”.

    2. === "Question"
    
            Déterminer la probabilité d’obtenir deux boules 
            
        === "Réponse"

            Obtenir deux boules blanches correspond à l’issue (A; A) :
            p 1 = 0, 6 × 0, 6 = 0, 36
    
    3.  === "Question" 
    
            Déterminer la probabilité une boule blanche et une boule rouge

        === "Réponse"

            Obtenir une boule blanche et une boule rouge correspond aux issues (A; B) et (B; A) :
            p 2 = 0, 24 + 0, 24 = 0, 48
    
    4. === "Question"
    
            Déterminer la probabilité au moins une boule blanche.
    
        === "Réponse"

            Obtenir au moins une boule blanche correspond aux issues (A; B), (B; A) et (A; A) :
            p 3 = 0, 24 + 0, 24 + 0, 36 = 0, 84

!!! note "Remarques"

    - Pour une expérience dont le nombre d’issues est supérieur à 2, le principe reste le même.
    - Pour une expérience dont le nombre de répétitions est supérieur à 2, le principe reste le même.

!!! prop "Propriété"

    Lorsqu’on répète $n$ fois de façon indépendante une expérience aléatoire dont les issues $A_1$ ,$A_2$, $\ldots$, $A_n$ ont pour probabilité $p(A_1)$, $p(A_2)$,$\ldots$, $p(A_n)$, la probabilité d’obtenir la suite d’issues $(A_1 , A_2 ,\ldots , A_n)$ est égale aux produits de leurs probabilités $p(A_1)\times p(A_2) \times\ldots\times p(A_n)$

!!! example "Exemple"

    On lance un dé à six faces 4 fois de suite. On considère les issues suivantes :
    
    - A : "On obtient un nombre pair."
    - B : "On obtient un 1."
    - C : "On obtient un 3 ou un 5."

    La probabilité d’obtenir la suite d’issues (A; B; A; C) est :

    \[p(A; B; A; C) = \dfrac{1}{2}\times\dfrac{1}{6}\times\dfrac{1}{2}\times\dfrac{1}{3}=\dfrac{1}{72}\]
   

## 2 - Épreuve de Bernoulli

!!! prop "Définition"

    Une épreuve de Bernoulli est une expérience aléatoire à deux issues que l’on peut nommer
    ”succès” ou ”échec”.

!!! prop "Définition"

    Une loi de Bernoulli est une loi de probabilité qui suit le schéma suivant : 

    1. la probabilité d’obtenir un succès est égale à $p$,
    2. la probabilité d’obtenir un échec est égale à $1-p$.
    
    $p$ est appelé le paramètre de la loi de Bernoulli.

!!! example "Exemples"

    Le jeu du pile ou face :

    - On considère par exemple comme succès ”obtenir pile” et comme échec ”obtenir face”.
    
    Dans ce cas, $p = \dfrac{1}{2}$

    - On lance un dé et on considère par exemple comme succès ”obtenir un six” et comme échec ”ne pas obtenir un six”. 
    
    Dans ce cas, $p = \dfrac{1}{6}$
    
    

!!! Note ""  

    Au succès, on peut associer le nombre 1 et à l’échec, on peut associer le nombre 0.

    Soit la variable aléatoire $X$ qui suit une loi de Bernoulli de paramètre $p$. Dans ce cas, la loi de probabilité de $X$ peut être présentée dans le tableau :

    <center>

    |$x_i$|$1$|$0$|
    |:-:|:-:|:-:|
    |$p(X = x_i )$|$p$ |$1 − p$|

    </center>

!!! prop "Propriété"

    Soit $X$ une variable aléatoire qui suit la loi de Bernoulli de paramètre $p$, alors :

    - Son espérance est $E(X) = p$
    - Sa variance est $V(X) = p(1 − p)$

!!! note "Démonstration"

    === "Calcul de l'espérance"

        $E(X) = 1 \times p(X = 1) + 0 \times p(X = 0)$

        $\phantom{E(X)} =  1 \times p + 0 \times (1 − p)$

        $\phantom{E(X)} = p$

    === "Calcul de la variance"
    
        $V(X) = (1 − E(X))^2 \times p(X = 1) + (0 − E(X))^2 \times p(X = 0)$

        $\phantom{V(X) }= (1 − p)^2 \times p + (0 − p)^2 \times (1 − p)$

        $\phantom{V(X) }= (1 − 2p + p^2 ) \times p + p^2 \times (1 − p)$

        $\phantom{V(X) }= p − 2p^2 + p^3 + p^2 − p^3$

        $\phantom{V(X) }= p − p^2$

        $\phantom{V(X) }= p(1 − p)$

## 3 - Schéma de Bernoulli, loi binomiale

### 3.1 - Schéma de Bernoulli

!!! prop "Définition"

    Un schéma de Bernoulli est la répétition de $n$ épreuves de Bernoulli identiques et indépendantes pour lesquelles la probabilité du succès est $p$.

!!! note "Remarque"

    Pour la répétition de $n$ épreuves de Bernoulli, l’univers est $\{0, 1, \ldots, n\}$ .

!!! example "Exemple"

    La répétition de 10 lancers d’une pièce de monnaie est un schéma de Bernoulli de paramètres $n = 10$ et $p = \dfrac{1}{2}$.
    

### 3.2 - Loi binomiale

!!! prop "Définition"

    On réalise un schéma de Bernoulli composé de n épreuves de Bernoulli identiques et indé-
    pendantes.
    Une loi binomiale est une loi de probabilité définie sur l’ensemble 0; 1; 2; ; n qui donne le
    nombre de succès de l’expérience.
    Notation
    n et p sont les paramètres de la loi binomiale et on note B(n; p).

!!! example "Exemple"

    Vidéo : https://youtu.be/b18_r8r4K2s
    On a représenté dans un arbre de probabilité les issues d’une expérience suivant un schéma
    de Bernoulli composé de 3 épreuves de Bernoulli de paramètre p.
    X est la variable aléatoire qui donne le nombre de succès.
    Succès
    p
    p
    1 − p
    Succès
    Echec
    Succès
    1 − p
    p
    Echec
    1 − p
    Succès
    p
    p
    1 − p
    p
    1 − p
    Succès
    Echec
    Succès
    Echec
    Echec
    1 − p
    Echec
    p
    1 − p
    Succès
    Echec
    On a par exemple :
    ã p(X = 3) = p 3 .
    En effet, en suivant les branches sur le haut de l’arbre, on arrive à 3 succès avec une
    probabilité égale à p × p × p.
    ã X = 2 correspond aux suites d’issues suivantes :
    (Succès ; Succès ; Échec) ; (Succès ; Échec ; Succès) et (Échec ; Succès ; Succès)
    Donc p(X = 2) = 3 × p 2 (1 − p)

### 3.3 - Expression de la loi binomiale à l’aide des coeﬀicients binomiaux

!!! prop "Propriété"

    On réalise une expérience suivant un schéma de Bernoulli de paramètres n et p.
    On associe à l’expérience la variable aléatoire X qui suit la loi binomiale B(n; p).
    Pour tout entier naturel k tel que 0 ⩽ k ⩽ n, la loi de probabilité de X est :
    ( )
    n k
    p(X = k) =
    p (1 − p) n−k
    k

!!! note "Démonstration"

    Vidéo : https://youtu.be/R45L_2gS8lU
    Un chemin comportant k succès (de probabilité p) comporte n − k échecs (de probabilité
    1 − p).
    Ainsi sa probabilité est égale à p k (1 − p) n−k .
    Le nombre de chemins menant à ( k ) succès revient à choisir la position des k succès parmi les
    n possibilités. Il est donc égal à nk .
    5Donc :
    ( )
    n k
    p(X = k) =
    p (1 − p) n−k
    k

!!! example "Méthode"
    Vidéo : https://youtu.be/1gMq2TJwSh0
    Une urne contient 5 boules gagnantes et 7 boules perdantes. Une expérience consiste à tirer
    au hasard 4 fois de suite une boule et de la remettre.
    On appelle X la variable aléatoire qui associe le nombre de tirages gagnants.
    1 . Prouver que X suit une loi binomiale.
    On répète 4 fois une expérience à deux issues : boules gagnantes (5 issues) ; boules
    perdantes (7 issues).
    Le succès est d’obtenir une boule gagnante.
    5
    La probabilité du succès sur un tirage est égale à p = .
    12
    5
    Donc la loi X suit la loi binomiale de paramètres n = 4 et p = .
    12
    2 . Déterminer la loi de probabilité de X.
    )
    ( ) ( ) k (
    5 4−k
    4
    5
    1 −
    p(X = k) =
    12
    12
    k
    ( ) ( ) k ( ) 4−k
    4
    5
    7
    =
    k
    12
    12
    3 . Calculer la probabilité d’obtenir 3 boules gagnantes.
    ( ) ( ) 3 ( ) 1 ( )
    7
    4
    125
    7
    4
    5
    =
    ×
    ×
    p(X = 3) =
    12
    12
    3
    1728 12
    3
    ( )
    4
    875
    875
    =
    ×
    =4 ×
    3
    20736
    20736
    875
    =
    ≃ 0, 17.
    5184

## 4 - La loi binomiale avec la calculatrice :

Liens vers les playlists pour chaque calculatrice.
Casio
Numworks
6
TI