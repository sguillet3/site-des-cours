---
title: Fonctions affines
---

<center>

# Fonctions affines

</center>

## 1. Définition

!!! prop "définition"

    Une fonction affine est une fonction  $f$  définie pour tout nombre réel $x$  par $f(x)=mx+p$ où  $m$  et  $p$  sont des nombres réels.

!!! Example "Exemple"

    La fonction  $f$  définie  par $f(x)=-2x+3$   est  une fonction affine  ($m=-2$ et $p=3$)

    1. 
		=== "Enoncé"
        
            Calculer l’image de $\dfrac{3}{4}$   par $f$

        === "Corrigé"
        
            L'image de $\dfrac{3}{4}$ par $f$ est $f\left(\dfrac{3}{4}\right)=-2\times \dfrac{3}{4}+3=\dfrac{3}{2}$

    2. 

        === "Enoncé"
        
            Déterminer l’antécédent de $-12$  par $f$
            
        === "Corrigé"
        
            Trouver l'antécédent de $-12$ par $f$ revient à résoudre $f(x)=-12$

            $\begin{array}{rl}
            -2x+3&=-12\\
            -2x&=-15\\
            x&=\dfrac{15}{2}\\
            \end{array}$
            
            Le seul antécédent de $-12$ par $f$ est $\dfrac{15}{2}$.

!!! Example "Cas particuliers" 

    - La fonction $g$ définie  par $g(x)=\dfrac{5}{3}x$  est une fonction linéaire car $m=\dfrac{5}{3}$  et  $p=0.$
    
    - La fonction $h$ définie  par $h(x)=-2$ est une fonction constante  car $m=0$  et $p=-2$.

##  2. Représentation graphique 

!!! prop "Propriété"

    Dans un repère, la représentation graphique d’une fonction affine est une droite $(d)$.
    
    - Le nombre  $m$  s’appelle le **coefficient directeur** ou **pente** de la droite $(d)$ :

        $m=\dfrac{y_A-y_B}{x_A-x_B}=\dfrac{f(x_A)-f(x_B)}{x_A-x_B}$ 

    - Le nombre  $p$  s’appelle l’ordonnée à l’origine de la droite $(d)$ :

        $(d)$ passe par le point de coordonnées $(0;p)$

Deux méthodes pour tracer la représentation graphique d’une fonction affine :

- On calcule les images de deux nombres

- On utilise l’ordonnée à l’origine et le coefficient directeur

!!! Example "Méthode"

    On souhaite tracer la droite représentative de la fonction affine $f(x)=3x-1$

    === "Méthode 1"

        - On calcule les images de deux nombres distincts :

            Par exemple $f(1)=3\times 1 -1=2$ et $f(-1)=3\times -1 -1=-4$

            la droite passe donc par les points de coordonnées $(1;2)$ et $(-1;-4)$
        
        - On place les deux points et on trace la droite

    === "Méthode 2"

        - L'ordonnée à l'origine est -1 donc la droite passe par le point de coordonnées $(0;-1)$

        - On utilise la pente qui vaut 3

!!! Example "Exemple  :"

    On note $\mathcal{C}_f$, $\mathcal{C}_g$ et  $\mathcal{C}_h$, les courbes représentatives respectives des fonctions $f$, $g$ et $h$ définies par :
    
    $f(x)=-2x+3$, $g(x)=\dfrac{5}{3}x$ et $h(x)=-2$.

    1. 
        === "Enoncé"
        
            Tracer les courbes de ces fonctions dans le repère ci-contre.

    2. 

        === "Enoncé"
        
            Le point $A(4 ; -5)$ appartient-il à la courbe $\mathcal{C}_f$, ? 
            
            Justifier votre réponse par un calcul.

    3. 
    
        === "Enoncé"
        
            Le point $B$ d’ordonnée 5 appartient à $\mathcal{C}_g$.  
            
            Calculer son abscisse.




!!! Note "Exercice :"

    On donne ci-dessous les courbes $\mathcal{C}_f$, $\mathcal{C}_g$ et  $\mathcal{C}_h$  de trois fonctions affines.

    ![Fonctions affines](images/affine1.png){width=500}
    
    Par lecture graphique, donner pour chacune son expression en fonction de $x$


## 3. Calculer l’expression de $f(x)$ en fonction de $x$ à l’aide de deux images de deux nombres

!!! Example "Exemple :"

    Déterminer la fonction affine  $f$  telle $f(-3)=0$ et $f(3)=6$.  



## 4. Sens de variation

!!! prop "Propriété"

    - Si  $m>0$  alors la fonction  $f$  est  croissante  sur $\mathbb{R}$ .

    - Si  $m=0$  alors la fonction  $f$  est  constante  sur  $\mathbb{R}$ .
    
    - Si  $m<0$  alors la fonction  $f$  est  décroissante  sur  $\mathbb{R}$ .

